// Imports: Dependencies
import { combineReducers } from 'redux';

// Initial State
const initialLoggedState = {
    loggedIn: false
  };
  const initialUserState = {
    userInfo: null
  };
  
  // Reducers (Modifies The State And Returns A New State)
  const userReducer = (state = initialUserState, action) => {
    switch (action.type) {
      // Increase Counter
      case 'STORE_USER': {
        return {
          // State
          ...state,
          // Redux Store
          userInfo: action.payload,
        }
      }
  
      // Default
      default: {
        return state;
      }
    }
  };
  
  // Reducers (Modifies The State And Returns A New State)
  const authReducer = (state = initialLoggedState, action) => {
    switch (action.type) {
      // Logged In
      case 'LOGGED_IN': {
        return {
          // State
          ...state,
          // Redux Store
          // Values: 'REGISTERED', 'AUTHENTICATED', false
          loggedIn: action.payload,
        }
      }
      // Default
      default: {
        return state;
      }
    }
  };

// Redux: Root Reducer
const rootReducer = combineReducers({
  authReducer: authReducer,
  userReducer: userReducer,
});

// Exports
export default rootReducer;