import React from 'react'
import { Text } from 'react-native'
export default function getDashboardNavigatorHeaderTitle(navigation) {
  const { routeName } = navigation.state
  let headerTitle
  
  if (routeName === 'Ready_for_pickup') {
    headerTitle = 'Ready for pickup'
  } else if (routeName === 'All_orders') {
    headerTitle = 'Orders'
  } else {
    headerTitle = routeName
  }

  return <Text style={{fontSize: 10, textAlign: 'center',fontFamily: 'MontserratRegular',color: '#a7adbf'}}>{headerTitle}</Text>
}
