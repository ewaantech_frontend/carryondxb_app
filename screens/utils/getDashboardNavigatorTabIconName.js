import React from 'react'
import { Image } from 'react-native'

export default function getDashboardNavigatorTabIconName(
  navigation,
  tintColor
) {
  const { routeName } = navigation.state
  if( navigation.isFocused() === true) {
    if (routeName === 'Upcoming') {
      return <Image
          source={require('../images/tab_icons/upcoming_active.png')}
          style={{ width: 20, height: 20, resizeMode: "contain",alignSelf: "center", marginTop: 2 }}
        />
    } else if(routeName === 'Ready_for_pickup') {
      return <Image
          source={require('../images/tab_icons/ready_active.png')}
          style={{ width: 20, height: 20, resizeMode: "contain",alignSelf: "center", marginTop: 2 }}
        />
    } else if(routeName === 'All_orders') {
      return <Image
          source={require('../images/tab_icons/all_active.png')}
          style={{ width: 20, height: 20, resizeMode: "contain",alignSelf: "center", marginTop: 2 }}
        />
    }  else {
      return <Image
          source={require('../images/tab_icons/profile_active.png')}
          style={{ width: 20, height: 20, resizeMode: "contain",alignSelf: "center", marginTop: 2 }}
        />
    }
  } else {
    if (routeName === 'Upcoming') {
      return <Image
          source={require('../images/tab_icons/upcoming.png')}
          style={{ width: 20, height: 20, resizeMode: "contain",alignSelf: "center", marginTop: 2 }}
        />
    } else if(routeName === 'Ready_for_pickup') {
      return <Image
          source={require('../images/tab_icons/ready.png')}
          style={{ width: 20, height: 20, resizeMode: "contain",alignSelf: "center", marginTop: 2 }}
        />
    } else if(routeName === 'All_orders') {
      return <Image
          source={require('../images/tab_icons/all.png')}
          style={{ width: 20, height: 20, resizeMode: "contain",alignSelf: "center", marginTop: 2 }}
        />
    }  else {
      return <Image
          source={require('../images/tab_icons/profile.png')}
          style={{ width: 20, height: 20, resizeMode: "contain",alignSelf: "center", marginTop: 2 }}
        />
    }
  }

}
