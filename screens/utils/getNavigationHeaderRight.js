import React from 'react'
// import { Feather } from '@expo/vector-icons'
import Icon from "react-native-vector-icons/Ionicons";


const onIconPress = (navigation, routeName) => {
  if (routeName === 'Profile') {
    navigation.navigate('EditProfile')
  }
}

export default function getNavigationHeaderRight(navigation) {
  const { routeName } = navigation.state.routes[navigation.state.index]
  let iconName

  if (routeName === 'Profile') {
    iconName = ''
  }

  return (
    <Icon
      name={iconName}
      color="#000"
      size={20}
      style={{ marginRight: 16 }}
      onPress={() => onIconPress(navigation, routeName)}
    />
  )
}
