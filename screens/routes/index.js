import {
    createAppContainer,
    createSwitchNavigator,
  } from 'react-navigation'
  import { createStackNavigator } from 'react-navigation-stack'
  import { createBottomTabNavigator } from 'react-navigation-tabs'
  import   Screens from '../views'
  import { defaultStackNavigatorHeaderStyle, tabBarOptions } from '../styles/common'
  import {
    getDashboardNavigatorHeaderTitle,
    getDashboardNavigatorTabIconName,
    getNavigationHeaderRight
  } from '../utils'
  import { PersistGate } from 'redux-persist/es/integration/react';
  import { Provider } from 'react-redux';
  import { store, persistor } from '../redux/store';
  import React from 'react';
  
  const MainStackNavigator = createStackNavigator(
    {
      // Initial Screen.
      Splash: {
        screen: Screens.Splash,
        navigationOptions: {
          headerShown: false,
        }
      },
      Login: {
        screen: Screens.Login,
        navigationOptions: {
          headerShown: false,
        }
      },
      Updateprofile: {
        screen: Screens.Updateprofile,
        navigationOptions: {
          headerShown: false,
        },
      },
      Passwordresetmessage: {
        screen: Screens.Passwordresetmessage,
        navigationOptions: {
          headerShown: false,
        },
      },
      

    },
    {
      defaultNavigationOptions: {
        headerBackTitle: null,
        ...defaultStackNavigatorHeaderStyle
      }
    }
  )
  
  const DashboardTabNavigator = createBottomTabNavigator(
    {
      Upcoming: {
        screen: Screens.UpcomingnewScreen,
      },
      Ready_for_pickup: {
        screen: Screens.PickupMainScreen,
        title: 'Orders',
        tabBarLabel: 'Details'
      },
      All_orders: {
        screen: Screens.AllOrders,
        title: 'Orders',
        tabBarLabel: 'Details'
      },
      Profile: {
        screen: Screens.Updateprofile,
        navigationOptions: {
          headerShown: false,
        },
      },
    },
    {
      defaultNavigationOptions: ({ navigation }) => {
        return {
          tabBarIcon: ({ tintColor }) =>{
            return getDashboardNavigatorTabIconName(navigation, tintColor)
          },
          tabBarLabel: () => {
            return getDashboardNavigatorHeaderTitle(navigation)
          },
        }
      },
      navigationOptions: ({ navigation }) => {
        return {
          headerShown: false,
          headerTitle: getDashboardNavigatorHeaderTitle(navigation),
          headerRight: getNavigationHeaderRight(navigation),
          gesturesEnabled: false,
          headerBackTitle: null,
          ...defaultStackNavigatorHeaderStyle
        }
      },
      tabBarOptions
    }
  )
  
  const DashboardStackNavigator = createStackNavigator({
    DashboardTabNavigator,
    order_details: {
      screen: Screens.OrderDetials,
      navigationOptions: {
        title: 'Order Details'
      },
    },
    Notification: {
      screen: Screens.Notification,
      navigationOptions: {
        title: 'Notifications'
      },
    },
  })
  
  const AppNavigator = createSwitchNavigator({
    MainStackNavigator,
    Dashboards: DashboardStackNavigator
  })
  
  const AppNavigatorContainer = createAppContainer(AppNavigator);
  
  export default class AppContainer extends React.Component {
  
    constructor(props) {
      super(props);
    }
  
    render() {
      return (
        <Provider store={store}>
        <PersistGate 
          loading={null}
          persistor={persistor}></PersistGate>
        <AppNavigatorContainer color={"grey"}/>
        </Provider>
      );
    }
  }
  