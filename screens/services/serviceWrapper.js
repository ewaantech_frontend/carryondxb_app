import { Component } from 'react';
import { ToastAndroid } from 'react-native';
import NetInfo from "@react-native-community/netinfo";

export const BASE_URL = 'https://con.ourdemo.online/';

export class serviceWrapper extends Component{

    root_url = BASE_URL + 'api/';

    post = (url, params, ContentType='application/json') => {
        this.checkConnection();
        const api_url = this.root_url + url;
        let data = params;
        if(ContentType == 'application/json')
            data = params.length != 0 ? JSON.stringify(params) : null;
        return fetch( api_url, {
            method: 'POST',
            headers: {
                'Content-Type': ContentType,
            },
            body: data,
        })
        .then((response) => response.json())
        .then((responseJson) => {
            return responseJson;
        })
        .catch((error) => {
            console.debug(error);
            return 'errors';
        });
    };

    post_test = (url, params, ContentType='application/json') => {
        this.checkConnection();
        const api_url = this.root_url + url;
        let data = params;
        if(ContentType == 'application/json')
            data = params.length != 0 ? JSON.stringify(params) : null;
        return fetch( api_url, {
            method: 'POST',
            headers: {
                'Content-Type': ContentType,
            },
            body: data,
        })
        .then((response) => response.text())
        .then((responseJson) => {
            return responseJson.substring(0, 500);
        })
        .catch((error) => {
            console.debug(error);
            return 'errors';
        });
    };

    checkConnection = () => {
        const unsubscribe = NetInfo.addEventListener(state => {
            if(state.type == 'unknown' || state.type == 'none') {
                ToastAndroid.showWithGravityAndOffset(
                    'Please check your network connection',
                    ToastAndroid.LONG,
                    ToastAndroid.BOTTOM,
                    25,
                    50,
                  );
            }
          });

        // to unsubscribe
        unsubscribe();
    }
}
