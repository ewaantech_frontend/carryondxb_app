import React, { Component } from "react";
import {  View, StyleSheet, TouchableOpacity, Alert, TextInput } from "react-native";
import { Container, Content, Text } from "native-base";
import { connect } from 'react-redux';
import {  ImageBackground,Image, KeyboardAvoidingView,
  } from 'react-native';
import { heightPercentageToDP as hp, widthPercentageToDP as wp, } from 'react-native-responsive-screen';
import { Item, Input, Icon, Label, Title, Body, Button } from 'native-base';


class Updateprofile extends Component {

  logout = () => {
    console.debug('das - sd');
    Alert.alert(
        'Logout',
        'Are you sure to logout?',
        [
          {
            text: 'Cancel',
            onPress: () => null,
            style: 'cancel',
          },
          {text: 'OK', onPress: () => this.logoutConfirm() },
        ],
        {cancelable: false},
      );
}

logoutConfirm = () => {
  const user = {};
  this.props.reduxLogin(false);
  this.props.reduxStoreUser(user);
  this.props.navigation.navigate('Login');
}

  render() {
    return (
      <Container>
             <View style={styles.MainContainer}>
             <Image
                        source={require('../..//images/logonew.png')}
                        style={{   width: wp('95%'), height:  hp('20%'),marginTop: hp('20%'), resizeMode: "contain" }}
                    />
                    </View>

                        {/* <View style={styles.bottomView}>
                        <Text style={{fontFamily: "Montserrat-Bold",fontSize: 20,color: "#434a5e"}}> Thank you !</Text>

                        <Text style={{ marginTop: "2%",fontFamily:"Montserrat-Regular",fontSize: 16,color: "#434a5e"}}> Try login with email and  password. </Text>
                            
                        </View> */}
                        <View style={styles.bottombutton}>
                        <TouchableOpacity onPress={() => {
                                this.logout();
                            }} style={{
                                backgroundColor: "#f25f1b",
                                width: "80%",
                                height: 50,
                            }}>
                                <Text style={{
                                    color: "#fff", alignSelf: "center",
                                    fontSize: 16,
                                    marginTop: 10,
                                    fontFamily: 'Montserrat-Medium'
                                    }}>LOGOUT </Text>
                            </TouchableOpacity>
                            </View>
      </Container>
    );
  }
}

const mapStateToProps = (state) => {
  return {
      userInfo: state.userReducer.userInfo,
      loggedIn: state.authReducer.loggedIn,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
      reduxStoreUser: (payload) => dispatch({
          type: 'STORE_USER',
          payload: payload,
      }),
      reduxLogin: (payload) => dispatch({
          type: 'LOGGED_IN',
          payload: payload,
      }),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Updateprofile);

const styles = StyleSheet.create({
  row: {
    flex: 1,
    flexDirection: "row",
     justifyContent:  'space-between',
    
  },
  inputWrap: {
    flex: 1,
    borderColor: "#cccccc",
    borderBottomWidth: 1,
    marginBottom: 10
  },
  inputdate: {
    fontSize: 14,
    marginBottom: -12,
    color: "#6a4595"
  },
  inputcvv: {
    fontSize: 14,
    marginBottom: -12,
    color: "#6a4595"
  },
  MainContainer: {
    flex: 1,
    alignItems: 'center',
   // height: "50%",
   
},
TextStyle: {
    color: '#0250a3',
    textAlign: 'center',
    fontSize: hp('4.2%'),
    marginTop: 10,
},
bottomView: {
   
    width: wp('95%'),
    height: hp('40%'),
    justifyContent: 'center',
    alignItems: 'center',
    bottom: 0,
    marginLeft:"2.5%",
    marginRight:"2.5%",
    marginTop: hp('20%')
},
bottombutton:{
    width: wp('95%'),
    height: hp('40%'),
    //backgroundColor: '#FFFFFF',
    justifyContent: 'center',
    alignItems: 'center',
    bottom: 0,
  
    // borderWidth:1,
    
    marginLeft:"2.5%",
    marginRight:"2.5%",
},
textStyle: {
    color: '#fff',
    fontSize: 22
}
});