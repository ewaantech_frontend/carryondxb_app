import React, { Component } from 'react';
import { heightPercentageToDP as hp, widthPercentageToDP as wp, } from 'react-native-responsive-screen'
import { Container, Content, List, ListItem, Text, Button, Body, View, Card } from 'native-base';
import { serviceWrapper } from '../../services/serviceWrapper';
import { cancellablePromise } from "../../services/cancellable-promise";
import { connect } from 'react-redux';
import { ListLoadingComponent, ListEmptyComponent } from '../components/ui'
import styled from 'styled-components/native'
import { FlatList } from 'react-native'

export const ListWrapper = styled.View`
  flex: 1;
`

class Notification extends Component {

  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      items: []
    }
  }

  componentDidMount() {
    this.list_notifications();
  }

  pendingPromises = [];

  appendPendingPromise = promise =>
    this.pendingPromises = [...this.pendingPromises, promise];

  removePendingPromise = promise =>
    this.pendingPromises = this.pendingPromises.filter(p => p !== promise);

    list_notifications = () => {
      console.debug('here');
      this.setState({ loading: true});
      const obj = new serviceWrapper();
      const data = {
        key: this.props.userInfo.key
      }
      const wrappedPromise = cancellablePromise(obj.post('notification_list', data));
      this.appendPendingPromise(wrappedPromise);
      wrappedPromise.promise
        .then((res) => {
          console.debug(res)
          this.setState({ loading: false});
          if(res !== 'errors') {
            if(res.status === true) {
              this.setState({ items: res.data });
            }
          }
        })
        .then(() => this.removePendingPromise(wrappedPromise))
      .catch(errorInfo => {
        this.setState({formLoading: false});
        if (!errorInfo.isCanceled) {
          this.removePendingPromise(wrappedPromise);
        }
      });
    }

    render_items = (item) => {
      // "created_at": "2020-03-27 13:07:30", 
      // "delivered": "NO", "id": 182, 
      // "message": "Order IdCON00142 is confirmed now", 
      // "status": "ACTIVE", 
      // "updated_at": "2020-03-27 13:07:30", 
      // "users_id": 60
      const order_id = item.message.split(' ')[1];
      const message = item.message.split('is')[0];
      const order_status = item.message.split('is')[1];
      return (
        <View>
          <ListItem style={{ borderBottomColor: '#00000000' }}>
            <Button style={{ backgroundColor: "#f25f1b", marginRight: '5%', borderRadius: 3, width: wp('25%'), height: hp('4%') }}>
              <Text style={{ textAlign: 'center', fontFamily: 'Montserrat-Medium' }}>{ order_id }</Text>
            </Button>
            <Text style={{ marginRight: '5%', color: '#434a5e', fontFamily: 'Montserrat-Medium' }}>StatusUpdate</Text>
            <Text style={{ marginLeft: '15%', color: 'black', backgroundColor: '#f5dc01', width: '2%', textAlign: 'center', borderRadius: 3, fontFamily: 'Montserrat-Medium' }}>New</Text>
          </ListItem>
          <Body>
            <Text style={{ fontFamily: 'Montserrat-Regular', color: '#434a5e', marginBottom: '3%', fontSize: hp('1.8%') }}>
              { message }
              <Text style={{ fontFamily: 'Montserrat-Regular', color: '#f25f1b', fontSize: hp('1.8%'), fontWeight: "bold" }}>
                { order_status }
              </Text>
            </Text>
          </Body>
        </View>
      )
    }

    onRefresh = () => {
      this.list_notifications();
    }

  render() {
    const { loading, items } = this.state;
    return (
      <Container>
        <Content>
          <List last>
            <ListWrapper>
              {loading && <ListLoadingComponent text="Loading Notifications" />}

              {!loading && items.length === 0 && (
                <ListEmptyComponent
                  showRefreshButton
                  text="No notifications to display."
                  onRefreshButtonPress={() => this.onRefresh()}
                />
              )}

              {!loading && items.length > 0 && (
                <FlatList
                  style={{ flex: 1 }}
                  data={items}
                  keyExtractor={item => items.indexOf(item).toString()}
                  renderItem={({ item }) => (
                    this.render_items(item)
                  )}
                  showsVerticalScrollIndicator={false}
                />
              )}
            </ListWrapper>
          </List>
        </Content>
      </Container>
    );
  }
}


const mapStateToProps = (state) => {
  // Redux Store --> Component
  return {
      userInfo: state.userReducer.userInfo,
      loggedIn: state.authReducer.loggedIn,
  };
};

export default connect(mapStateToProps)(Notification);