import React from 'react'
import PropTypes from 'prop-types'
import { Formik } from 'formik'
import * as yup from 'yup'
import { FormField } from '../ui'
import FormContainer from './FormContainer'

import { serviceWrapper } from '../../services/serviceWrapper';
import { cancellablePromise } from "../../services/cancellable-promise";
import { ToastAndroid } from 'react-native';

const initialValues = {
  Ifsccode: '',
  email: '',
  bankname: '',
  acname: '',
  branch: '',
  acnumber:'',
  reenterac:'',
  upiid:'',
  phonepay:'',
  googlepay:'',
  phonepay: ''
}

const validationSchema = yup.object().shape({
  Ifsccode: yup.string().required('Ifsc code is required'),
  branch: yup.string().required('Branch name is required'),
  bankname: yup.string().required('Bank name is required'),
  acname: yup.string().required('Account holder name is required'),
  acnumber: yup.string().required('Account number is required'),
  reenterac: yup.string().required('Account number confirmation is required').test('passwords-match', 'Account number confirmation is not matching', function(value) {
    return this.parent.acnumber === value;
  }),
  
});

let pendingPromises = [];

const appendPendingPromise = promise =>
  pendingPromises = [...pendingPromises, promise];

const removePendingPromise = promise =>
  pendingPromises = pendingPromises.filter(p => p !== promise);

bankDetails = (text) => {
  return new Promise(function(resolve, reject) {
    const obj = new serviceWrapper();
      const data = {
          "ifsc": text,
      }
      const wrappedPromise = cancellablePromise(obj.post('auth/checkifsc', data));
      appendPendingPromise(wrappedPromise);
      wrappedPromise.promise
        .then((res) => {
          console.debug(res)
          // this.setState({formLoading: false});
          if(res !== 'errors') {
            if(res.status === true) {
              resolve(res);
            }
          }
        })
        .then(() => removePendingPromise(wrappedPromise))
      .catch(errorInfo => {
        // this.setState({formLoading: false});
        reject(errorInfo);
        if (!errorInfo.isCanceled) {
          removePendingPromise(wrappedPromise);
        }
      });
    });
}

export default class BankDetailsForm extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      activeFocus: '',
      loader: this.props.loading
    }
  }

  render() {
    let data = {};
    let loaderItem = this.props.loading;
    if(typeof this.props.initialValues != 'undefined' && this.props.initialValues.acnumber != '') {
      data = this.props.initialValues;
      loaderItem = false;
    } else {
      data = initialValues
    }
  return (
    // <KeyboardAvoidingView behavior="padding">
    <Formik
      initialValues={data}
      validationSchema={validationSchema}
      onSubmit={values => this.props.onSubmit(values)}
      >{(props) => (
        <FormContainer
          loading={ loaderItem }
          submitButtonText="SUBMIT"
          onSubmitButtonPress={() => props.handleSubmit()}
          validCheck={props.isValid}
          dirty={props.dirty || this.props.dirty}
        >
          <FormField
            label="IFSC Code *"
            uniqueid="ifsccode"
            focusedfield={this.state.activeFocus}
            value={props.values.Ifsccode}
            onChangeText={text => {
              props.setFieldValue('Ifsccode', text);
              if(text.length < 11) {
                props.setFieldValue('bankname', '');
                props.setFieldValue('branch', '');
                return false;
              }
              this.setState({loader: true});
              const that = this;
              bankDetails(text).then(function(val) {
                that.setState({loader: false});
                if(typeof val.data.error != 'undefined' && val.data.error == 'IFSC Code not found') {
                  ToastAndroid.showWithGravityAndOffset(
                    val.data.error,
                    ToastAndroid.LONG,
                    ToastAndroid.BOTTOM,
                    25,
                    50,
                  );
                  props.setFieldValue('Ifsccode', text);
                } else {
                  props.setFieldValue('bankname', val.data.bank);
                  props.setFieldValue('branch', val.data.branch);
                }
             });
            }
            }
            error={props.errors.Ifsccode}
            onFocus={this.onShow}
          />

          <FormField
            label="Bank Name *"
            uniqueid="bankname"
            focusedfield={this.state.activeFocus}
            value={props.values.bankname}
            onChangeText={text => props.setFieldValue('bankname', text)}
            error={props.errors.bankname}
            onFocus={this.onShow}
          />
          <FormField
            label="Branch Name *"
            uniqueid="branchname"
            focusedfield={this.state.activeFocus}
            value={props.values.branch}
            onChangeText={text => props.setFieldValue('branch', text)}
            error={props.errors.branch}
            onFocus={this.onShow}
          />
  
           <FormField
            label="A/C Holder Name *"
            uniqueid="acname"
            focusedfield={this.state.activeFocus}
            value={props.values.acname}
            onChangeText={text => props.setFieldValue('acname', text)}
            error={props.errors.acname}
            onFocus={this.onShow}
           /> 

          <FormField
            label="A/C Number *"
            uniqueid="acnumber"
            keyboardType={'numeric'}
            focusedfield={this.state.activeFocus}
            value={props.values.acnumber}
            onChangeText={text => props.setFieldValue('acnumber', text)}
            error={props.errors.acnumber}
            onFocus={this.onShow}
          />
           <FormField
            label="Re Enter A/C Number *"
            uniqueid="reenterac"
            keyboardType={'numeric'}
            focusedfield={this.state.activeFocus}
            value={props.values.reenterac}
            onChangeText={text => props.setFieldValue('reenterac', text)}
            error={props.errors.reenterac}
            onFocus={this.onShow}
          />

          <FormField
            label="Google Pay"
            uniqueid="googlepay"
            focusedfield={this.state.activeFocus}
            value={props.values.googlepay}
            onChangeText={text => props.setFieldValue('googlepay', text)}
            error={props.errors.googlepay}
            onFocus={this.onShow}
          />

          <FormField
            label="Phone Pay "
            uniqueid="phonepay"
            focusedfield={this.state.activeFocus}
            value={props.values.phonepay}
            onChangeText={text => props.setFieldValue('phonepay', text)}
            error={props.errors.phonepay}
            onFocus={this.onShow}
          />

        </FormContainer>
      )}
    </Formik>
    // </KeyboardAvoidingView>
  )
  }

  onShow = (id) => {
    this.setState({ activeFocus: id });
  }
}

BankDetailsForm.propTypes = {
  loading: PropTypes.bool.isRequired,
  onSubmit: PropTypes.func.isRequired
}
