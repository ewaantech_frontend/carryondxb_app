import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import { Formik } from 'formik'
import * as yup from 'yup'
import { FormField } from '../ui'
import FormContainer from './FormContainer'
import SelectCountry from './SelectCountry'
import SelectStates from './SelectStates'
import SelectgenderType from './SelectgenderType'

const initialValues = {
  firstName: '',
  lastName: '',
  email: '',
  address: '',
  city: '',
  state: '',
  country: '',
  visible: false,
  picked: null,
  gender: ''
}

const validationSchema = yup.object().shape({
  firstName: yup.string().required('first name is required'),
  lastName: yup.string().required('last name is required'),
  email: yup
    .string()
    .required('email address is required')
    .email('enter a valid email address'),
})


export default class EditProfileForm extends React.Component {
  
  constructor(props) {
    super(props);
    this.state = {
      activeFocus: ''
    }
  }

  onShow = (id) => {
    this.setState({ activeFocus: id });
  }

  render() {
    return (
      <Formik
        initialValues={initialValues}
        validationSchema={validationSchema}
        onSubmit={values => this.props.onSubmit(values)}
        >{(props) => (
          <FormContainer
            loading={this.props.loading}
            submitButtonText="NEXT"
            onSubmitButtonPress={() => props.handleSubmit()}
            validCheck={props.isValid}
            dirty={props.dirty}
          >
            <FormField
              label="First Name"
              value={props.values.firstName}
              onChangeText={(text) => props.setFieldValue('firstName', text)}
              error={props.errors.firstName}

              uniqueid="f_name"
              focusedfield={this.state.activeFocus}
              onFocus={this.onShow}
            />

            <FormField
              label="Last Name"
              value={props.values.lastName}
              onChangeText={text => props.setFieldValue('lastName', text)}
              error={props.errors.lastName}

              uniqueid="l_name"
              focusedfield={this.state.activeFocus}
              onFocus={this.onShow}
            />

            <SelectgenderType
              value={props.values.gender}
              onChangeText={text => {
                props.setFieldValue('gender', text)
              }}
              error={props.errors.gender}

              uniqueid="gender"
              focusedfield={this.state.activeFocus}
              onFocus={this.onShow}
              />

            <FormField
              label="Email"
              keyboardType="email-address"
              value={props.values.email}
              onChangeText={text => props.setFieldValue('email', text)}
              error={props.errors.email}

              uniqueid="email"
              focusedfield={this.state.activeFocus}
              onFocus={this.onShow}
            />

            <SelectCountry
              value={props.values.country}
              onChangeText={text => {
                props.setFieldValue('country', text)
              }}
              error={props.errors.country}

              uniqueid="country"
              focusedfield={this.state.activeFocus}
              onFocus={this.onShow}
              />

          {this.props.zone == 'NORMAL' &&
            <SelectStates
              value={props.values.state}
              onChangeText={text => {
                props.setFieldValue('state', text)
              } }
              error={props.errors.state}

              uniqueid="state"
              focusedfield={this.state.activeFocus}
              onFocus={this.onShow}
              />
          }

            {/* <FormField
              label="Address"
              value={props.values.address}
              onChangeText={text => props.setFieldValue('address', text)}
              error={props.errors.address}

              uniqueid="address"
              focusedfield={this.state.activeFocus}
              onFocus={this.onShow}
            /> */}

            <FormField
              label="City"
              value={props.values.city}
              onChangeText={text => props.setFieldValue('city', text)}
              error={props.errors.city}

              uniqueid="city"
              focusedfield={this.state.activeFocus}
              onFocus={this.onShow}
            />

          </FormContainer>
        )}
      </Formik>
    )
  }
}

EditProfileForm.propTypes = {
  loading: PropTypes.bool.isRequired,
  onSubmit: PropTypes.func.isRequired
}
