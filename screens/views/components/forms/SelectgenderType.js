import React from 'react'
import { View, TouchableOpacity, Keyboard } from 'react-native'
import ModalFilterPicker from '../modal-picker'
import { FormField } from '../ui'
import { connect } from 'react-redux';

class SelectGenderType extends React.Component {
  constructor (props) {
    super(props);

    this.state = {
      visible: false,
      picked: this.props.value,
      error: this.props.error,
      options: [{key: 'MALE', label: 'MALE'}, {key: 'FEMALE', label: 'FEMALE'}, {key: 'TRANSGENDER', label: 'TRANSGENDER'}],
      mapped: {},
    };
    
  }
  
  pendingPromises = [];

  appendPendingPromise = promise =>
    this.pendingPromises = [...this.pendingPromises, promise];

  removePendingPromise = promise =>
    this.pendingPromises = this.pendingPromises.filter(p => p !== promise);

  componentDidMount() {
    this.pendingPromises.map(p => p.cancel());
  }

  render() {
    const { visible } = this.state;

    return (
      <View >
        <TouchableOpacity onPress={this.onShow}>
            <FormField
            label="Select Gender"
            value={ this.state.picked }
            onChangeText={(text) => this.props.onChangeText(text) }
            error={this.state.error}
            onFocus={this.onShow}

            uniqueid={this.props.uniqueid}
            focusedfield={this.state.focusedfield}
            />
        </TouchableOpacity>
        <ModalFilterPicker
          visible={visible}
          onSelect={this.onSelect}
          onCancel={this.onCancel}
          options={this.state.options}
        />
      </View>
    );
  }

  onShow = (id) => {
    this.props.onFocus(id);
    this.setState({ visible: true });
    Keyboard.dismiss()  
  }

  onSelect = (picked) => {
    this.setState({
      picked: picked,
      visible: false,
      error: '',
    });
    this.props.onChangeText(picked);
  }

  onCancel = () => {
    this.setState({
      visible: false
    });
  }
}


const mapStateToProps = (state) => {
    // Redux Store --> Component
    return {
        userInfo: state.userReducer.userInfo,
        loggedIn: state.authReducer.loggedIn,
    };
  };
  
  export default connect(mapStateToProps)(SelectGenderType);