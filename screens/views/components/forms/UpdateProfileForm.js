import React from 'react'
import PropTypes from 'prop-types'
import { Formik } from 'formik'
import * as yup from 'yup'
import { FormField } from '../ui'
import FormContainer from './FormContainer'

const initialValues = {
  firstname: '',
  lastname: '',
  email: '',
  referralcode: ''
}

const validationSchema = yup.object().shape({
  firstname: yup.string().required('First name is required'),
  lastname: yup.string().required('Last name is required'),
  email: yup
    .string()
    .required('email address is required')
    .email('enter a valid email address'),
  
});

export default class updateProfileForm extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      activeFocus: ''
    }
  }

  render() {
  return (
    // <KeyboardAvoidingView behavior="padding">
    <Formik
      initialValues={this.props.initialValues}
      validationSchema={validationSchema}
      onSubmit={values => this.props.onSubmit(values)}
      >{props => (
        <FormContainer
          loading={this.props.loading}
          submitButtonText="SUBMIT"
          onSubmitButtonPress={() => props.handleSubmit()}
          validCheck={props.isValid}
          dirty={true}
        >

          <FormField
            label="First Name *"
            uniqueid="firstname"
            focusedfield={this.state.activeFocus}
            value={props.values.firstname}
            onChangeText={text => props.setFieldValue('firstname', text)}
            error={props.errors.firstname}
            onFocus={this.onShow}
          />
          <FormField
            label="Last Name *"
            uniqueid="lastname"
            focusedfield={this.state.activeFocus}
            value={props.values.lastname}
            onChangeText={text => props.setFieldValue('lastname', text)}
            error={props.errors.lastname}
            onFocus={this.onShow}
          />
  
           <FormField
            label="Email *"
            uniqueid="email"
            focusedfield={this.state.activeFocus}
            value={props.values.email}
            onChangeText={text => props.setFieldValue('email', text)}
            error={props.errors.email}
            onFocus={this.onShow}
           /> 

        {props.values.referred == null &&
          <FormField style={{ paddingBottom:30 }}
            label="Enter referral code If you have any "
            uniqueid="referralcode"
            focusedfield={this.state.activeFocus}
            value={props.values.referralcode}
            onChangeText={text => props.setFieldValue('referralcode', text)}
            error={props.errors.referralcode}
            onFocus={this.onShow}
          />
      }

        </FormContainer>
      )}
    </Formik>
    // </KeyboardAvoidingView>
  )
  }

  onShow = (id) => {
    this.setState({ activeFocus: id });
  }
}

updateProfileForm.propTypes = {
  loading: PropTypes.bool.isRequired,
  onSubmit: PropTypes.func.isRequired
}
