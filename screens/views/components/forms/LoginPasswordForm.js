import React from 'react'
import PropTypes from 'prop-types'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import styled from 'styled-components/native'
import { Formik } from 'formik'
import * as yup from 'yup'
import { CustomActivityIndicator, CustomButton } from '../ui'
import { ActionText } from './FormContainer'
import { StyleSheet, Text,  TouchableOpacity } from 'react-native'
import OTPInputView from '@twotalltotems/react-native-otp-input'
import { Image } from 'react-native';

const Wrapper = styled.View`
  padding: 16px;
  paddingTop: 76px;
`

const initialValues = {
  password: '',
}

const passRegExp = /^(?=.*\d).{4,8}$/

const validationSchema = yup.object().shape({
  password: yup
  .string()
  .required('Password is required')
  .matches(passRegExp, 'Password must be between 4 and 8 digits long and include at least one numeric digit.')
});

export default function LoginPasswordForm({ loading, onSubmit, onSignUpPress }) {
  return (
    <Formik
      initialValues={initialValues}
      validationSchema={validationSchema}
      onSubmit={values => onSubmit(values)}
      render={props => (
        <KeyboardAwareScrollView enableOnAndroid>
          
          <Image
            style={{width: 70, height: 70, marginBottom:20, marginLeft: "40%", marginTop: 10}}
            source={require('../../../assets/images/logo_256.png')}
          />
            <Text style={{ color:"#000000", textAlign: "center", fontSize: 20,fontFamily:"ArialRoundedMTBold"}}>Enter your PIN</Text>
            <OTPInputView
                style={{width: '60%', height: 60, fontSize: 40, marginLeft: "20%", fontWeight: 'bold'}}
                pinCount={4}
                keyboardType="phone-pad"
                autoFocusOnLoad
                onCodeChanged = {code => { props.setFieldValue('password', code) }}
                codeInputFieldStyle={styles.underlineStyleBase}
                codeInputHighlightStyle={styles.underlineStyleHighLighted}
                onCodeFilled = {(code => { 
                  props.setFieldValue('otp', code);
                  onSubmit({password: code});
                })}
            />
          <Wrapper>
            <CustomButton text={"LOGIN"} onPressAction={ () => onSubmit({password: props.password}) }/>
            {loading && <CustomActivityIndicator />}
            <TouchableOpacity   onPress={() => onSignUpPress()}>
            <ActionText  style={{ paddingRight:"10%"}} >
              Reset your pin?
            </ActionText>
          </TouchableOpacity>
          </Wrapper>
        </KeyboardAwareScrollView>
      )}
    />
  )
}

const styles = StyleSheet.create({
  borderStyleBase: {
    width: 30,
    height: 45
  },

  borderStyleHighLighted: {
    borderColor: "#696969",
    fontSize: 19,
  },

  underlineStyleBase: {
    width: 30,
    height: 45,
    borderWidth: 0,
    borderBottomWidth: 2,
    fontSize: 19,
  },
  underlineStyleHighLighted: {
    borderColor: "#696969",
    fontSize: 19,
  },

});

LoginPasswordForm.propTypes = {
  loading: PropTypes.bool.isRequired,
  onSubmit: PropTypes.func.isRequired,
  onSignUpPress: PropTypes.func.isRequired
}
