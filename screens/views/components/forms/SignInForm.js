import React from 'react'
import PropTypes from 'prop-types'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import styled from 'styled-components/native'
import { Formik } from 'formik'
import * as yup from 'yup'
import { CustomActivityIndicator, CustomButton } from '../ui'
import { Text } from 'native-base';
import { Image, StyleSheet, View } from 'react-native';
import FlagPhoneNumberForm from './FlagPhoneNumberForm';

const Wrapper = styled.View`
  padding: 26px;
  paddingTop: 76px; 
`
const initialValues = {
  mobile: '',
}

const phoneRegExp = /[2-9]{1}\d{9}/

const validationSchema = yup.object().shape({
  mobile: yup
  .string()
  .required('Mobile number is required')
  .matches(phoneRegExp, 'Invalid Mobile number. Please enter 10 digit valid mobile number')
})

const Error = styled.Text`
  font-family: 'SFProTextRegular';
  font-size: 12;
  color: #b00727;
  margin: 4px 8px;
`

export default class SignInForm extends React.Component {

  constructor(props) {
    super(props);
  }

  render() {
    const {navigation} = this.props;
    return (
      <Formik
        initialValues={initialValues}
        validationSchema={validationSchema}
        onSubmit={values => this.props.onSubmit(values)}
        >{(props) => (
          <KeyboardAwareScrollView enableOnAndroid>
            <Image
              style={styles.image}
              source={require('../../../assets/images/logo_256.png')}
            />
            <Text style={styles.welcome_text}>Welcome to AlmsApp</Text>
            <View style={{marginTop:10}}>
              <Text style={styles.mobile_enter}>Enter your Mobile Number</Text>
              <FlagPhoneNumberForm onPress={(mobile) =>{ props.setFieldValue('mobile', mobile)} }/>
              {!!props.errors.mobile && <Error>{props.errors.mobile}</Error>}
              <Wrapper>
                <CustomButton text={"NEXT"} onPressAction={() =>{ props.handleSubmit(); }}/>
              </Wrapper>

              {this.props.loading && <CustomActivityIndicator />}
            </View>

          </KeyboardAwareScrollView>
        )}
        
      </Formik>
    )
  }
}

const styles = StyleSheet.create({
  welcome_text: {
    color:"black", textAlign: "center", fontSize: 30,
    marginBottom: "2%",fontFamily:"ArialRoundedMTBold"
  },
  mobile_enter: {
    color:"black", textAlign: "center",  fontSize: 19,
    marginBottom: "2%",fontFamily:"ArialRoundedMTBold"
  },
  image: {
    width: 70, height: 70, marginBottom:20, 
    marginLeft: "40%", marginTop: 20
  },
  text:{
    fontFamily: 'ArialRoundedMTBold',
    fontSize: 13,
    color: "#8a8a8f",
    paddingLeft:15,
    paddingRight:15,
    lineHeight: 15,
    paddingBottom:20,
    textAlign: "center"
  },

  actionButtons: {
    flexDirection: "row",
    alignSelf: "flex-end"
  },
  modalContent: {
    backgroundColor: 'white',
    padding: 22,
    borderRadius: 4,
    borderColor: 'rgba(0, 0, 0, 0.1)'
  },
  modalMainContent: {
    justifyContent: 'flex-end',
    margin: 0,
  },
  button: {
    backgroundColor: '#EAEAEA',
    padding: 12,
    alignSelf: 'flex-start',
    borderRadius: 0,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  buttonyes: {
    backgroundColor: '#EAEAEA',
    padding: 12,
    flexDirection: 'row',
    alignSelf: 'flex-end',
    borderRadius: 0,
    borderColor: 'rgba(0, 0, 0, 0.1)',
    marginLeft: 5
  },
  modaltext:
  {
    color:"black", 
    textAlign: "center",
    fontSize: 15,
    marginBottom: "2%",
    fontFamily:"ArialRoundedMTBold"
  }
})

SignInForm.propTypes = {
  loading: PropTypes.bool.isRequired,
  onSubmit: PropTypes.func.isRequired,
  onSignUpPress: PropTypes.func.isRequired
}
