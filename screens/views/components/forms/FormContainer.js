import React from 'react'
import PropTypes from 'prop-types'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import styled from 'styled-components/native'
import { RegularButton, CustomActivityIndicator, CustomButton } from '../ui'

const Wrapper = styled.View`
  padding: 16px;
`

export const ActionText = styled.Text`
  font-family: 'ArialRoundedMTBold';
  font-size: 17;
  text-align: right;
  color: #8a8a8f;
  margin-top: 16;
 
` 
 /*textDecorationLine: underline;*/
 // color: #2EBC8F;


export default function FormContainer({
  actionText,
  submitButtonText,
  onSubmitButtonPress,
  loading,
  children,
  validCheck,
  dirty
}) {
  if (validCheck && dirty) {
    return (
      <KeyboardAwareScrollView enableOnAndroid>
        <Wrapper>
          {children}
  
          {/* <RegularButton
            text={submitButtonText}
            onPress={() => onSubmitButtonPress()}
          /> */}
  
          <CustomButton
            text={submitButtonText}
            onPressAction={() => onSubmitButtonPress()}
          />
  
          {actionText && <ActionText>{actionText}</ActionText>}
  
          {loading && <CustomActivityIndicator />}
        </Wrapper>
      </KeyboardAwareScrollView>
    )
  } else {
    return (
      <KeyboardAwareScrollView enableOnAndroid>
        <Wrapper>
          {children}
  
          {/* <RegularButton
            text={submitButtonText}
            onPress={() => onSubmitButtonPress()}
          /> */}
  
          <RegularButton
            text={submitButtonText}
          />
  
          {actionText && <ActionText>{actionText}</ActionText>}
  
          {loading && <CustomActivityIndicator />}
        </Wrapper>
      </KeyboardAwareScrollView>
    )    
  }
}

FormContainer.propTypes = {
  actionText: PropTypes.string,
  submitButtonText: PropTypes.string.isRequired,
  onSubmitButtonPress: PropTypes.func.isRequired,
  loading: PropTypes.bool.isRequired,
  children: PropTypes.node.isRequired
}
