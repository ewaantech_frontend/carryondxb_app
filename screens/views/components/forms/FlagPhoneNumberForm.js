import React, { Component } from "react";
import { View, StyleSheet } from "react-native";
import { Item, Input } from 'native-base';
import styled from 'styled-components/native'
const Flag = styled.Image.attrs({
  source: require('../../../assets/images/flag.png'),
})``
export default class FlagPhoneNumberForm extends Component {
  render() {
    return (
      <View style={styles.row}>
        <View style={{width: '25%'}}>
            <Item disabled>
            <Flag />
              <Input disabled placeholder="+91"
              style={{ fontSize: 20,}}
              iconName="ios-heart"
              iconColor="#f42725" />
            </Item>
        </View>
        <View style={{width: '75%'}}>
                <Item success  >
              <Input
              onChangeText={ (text) => this.props.onPress(text) }
              keyboardType="phone-pad"
              style={{ borderBottomColor: 'red',fontSize: 20,}}/>
            </Item>
        </View>
       
      </View>
    );
  }
}

const styles = StyleSheet.create({
  row: {
    flex: 1,
    flexDirection: "row"
  },
  inputWrap: {
    flex: 1,
    marginBottom: 10
  },
  inputdate: {
    fontSize: 14,
    marginBottom: -12,
    color: "#6a4595"
  },
  inputcvv: {
    fontSize: 14,
    marginBottom: -12,
    color: "#6a4595"
  }
});