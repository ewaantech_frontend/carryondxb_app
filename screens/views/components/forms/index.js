import EditProfileForm from './EditProfileForm'
import SignInForm from './SignInForm'
import SignUpForm from './SignUpForm'
import OtpForm from './OtpForm'
import PasswordForm from './PasswordForm'
import BankDetailsForm from './BankDetailsForm'
//import LoginForm from './LoginForm'
import LoginPasswordForm from './LoginPasswordForm'
import ForgotPasswordForm from './ForgotPasswordForm'
import UpdateProfileForm from './UpdateProfileForm'

export { EditProfileForm, SignInForm, SignUpForm,
       OtpForm, PasswordForm, BankDetailsForm,  ForgotPasswordForm , LoginPasswordForm, UpdateProfileForm }
