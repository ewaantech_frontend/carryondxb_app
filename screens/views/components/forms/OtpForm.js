import React from 'react'
import PropTypes from 'prop-types'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import styled from 'styled-components/native'
import {  CustomActivityIndicator } from '../ui'
import OTPInputView from '@twotalltotems/react-native-otp-input'
import { StyleSheet, Text } from 'react-native'
import { Image } from 'react-native';

const Wrapper = styled.View`
  padding: 16px;
`

export default function OtpForm({ loading, onSubmit, onSignUpPress, codeSms }) {
  return (
        <KeyboardAwareScrollView enableOnAndroid>
          <Wrapper>
          <Image
            style={{width: 70, height: 70, marginBottom:20, marginLeft: "40%", marginTop: 10}}
            source={require('../../../assets/images/logo_256.png')}
          />
            <Text style={{ color:"#000000", textAlign: "center", fontSize: 20,fontFamily:"ArialRoundedMTBold" }}>We have sent you an OTP</Text>
            <OTPInputView
                style={{width: '80%', height: 60, fontSize: 60, marginLeft: "10%"}}
                pinCount={6}
                keyboardType="phone-pad"
                code={codeSms}
                onCodeChanged = {code => { onSubmit({otp: code}) }}
                autoFocusOnLoad
                codeInputFieldStyle={styles.underlineStyleBase}
                codeInputHighlightStyle={styles.underlineStyleHighLighted}
                onCodeFilled = {(code => {
                  if(code.length == 6) {
                    onSubmit({otp: code});
                  }
                })}
            />
            {loading && <CustomActivityIndicator />}
          </Wrapper>
        </KeyboardAwareScrollView>
  )
}

OtpForm.propTypes = {
  loading: PropTypes.bool.isRequired,
  onSubmit: PropTypes.func.isRequired,
  onSignUpPress: PropTypes.func.isRequired
}


const styles = StyleSheet.create({
    borderStyleBase: {
      width: 30,
      height: 45
    },
  
    borderStyleHighLighted: {
      borderColor: "#696969",
      fontSize: 19,
    },
  
    underlineStyleBase: {
      width: 30,
      height: 45,
      borderWidth: 0,
      borderBottomWidth: 2,
      fontSize: 19,

    },
  
    underlineStyleHighLighted: {
      borderColor: "#696969",
      fontSize: 19,
    },
  });