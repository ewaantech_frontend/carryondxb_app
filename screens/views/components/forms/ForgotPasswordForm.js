
import React from 'react'
import PropTypes from 'prop-types'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import styled from 'styled-components/native'
import { Formik } from 'formik'
import * as yup from 'yup'
import { RegularButton, CustomActivityIndicator } from '../ui'
import OTPInputView from '@twotalltotems/react-native-otp-input'
import {StyleSheet, Text } from 'react-native'

const Wrapper = styled.View`
  padding: 16px;
`

const profilePhotoDimensions = 100
const ProfilePhoto = styled.Image.attrs({
  source: require('../../../assets/images/profile.jpeg')
})`
  width: ${profilePhotoDimensions};
  height: ${profilePhotoDimensions};
  border-radius: ${profilePhotoDimensions / 2};
  border-width: 5;
  border-color: #fff;
  align-self: center;
  margin-bottom: 16;
`

const initialValues = {
  otp: '',
}

const otpRegExp = /^[0-9]{4}$/

const validationSchema = yup.object().shape({
  otp: yup
  .string()
  .required('Otp is required')
  .matches(otpRegExp, 'Enter 4 Digit OTP send to your mobile number')
 
});

export default function ForgotPasswordForm({ loading, onSubmit, sendOtp }) {
  return (
    <Formik
      initialValues={initialValues}
      validationSchema={validationSchema}
      onSubmit={values => onSubmit(values)}
      render={props => (
        <KeyboardAwareScrollView enableOnAndroid>
          <Wrapper>
            <ProfilePhoto />

            <Text onPress={() => sendOtp() } style={{color:"#696969", textAlign: "center", fontSize: 19,fontWeight: 'bold'}}>Send OTP</Text>
            <OTPInputView
                style={{width: '100%', height: 200}}
                pinCount={4}
                autoFocusOnLoad
                onCodeChanged = {code => { props.setFieldValue('otp', code) }}
                codeInputFieldStyle={styles.underlineStyleBase}
                codeInputHighlightStyle={styles.underlineStyleHighLighted}
                onCodeFilled = {(code => { props.setFieldValue('otp', code) })}
            />

            <CustomButton text={"LOGIN"} onPressAction={ () => onSubmit({otp: props.otp}) }/>
            <RegularButton text="Proceed" onPress={() => props.handleSubmit()} />

            {loading && <CustomActivityIndicator />}
          </Wrapper>
        </KeyboardAwareScrollView>
      )}
    />
  )
}

ForgotPasswordForm.propTypes = {
  loading: PropTypes.bool.isRequired,
  onSubmit: PropTypes.func.isRequired,
}


const styles = StyleSheet.create({
    borderStyleBase: {
      width: 30,
      height: 45
    },
  
    borderStyleHighLighted: {
      borderColor: "#03DAC6",
    },
  
    underlineStyleBase: {
      width: 30,
      height: 45,
      borderWidth: 0,
      borderBottomWidth: 1,
    },
  
    underlineStyleHighLighted: {
      borderColor: "#03DAC6",
    },
  });