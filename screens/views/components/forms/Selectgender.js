import React, { Component } from 'react';
import { Dropdown } from 'react-native-material-dropdown';
import { View, TouchableOpacity } from 'react-native'
import { connect } from 'react-redux';

export default class Selectgender extends Component {
  constructor(props) {
    super(props)
  }
  render() {
    let data = [{
      value:'MALE',
    },{
      value:'FEMALE',
    },{
      value:'TRANSGENDER',
    }];
 
    return(
      <View style={{paddingLeft:10}}>
      <Dropdown
        label='Select Gender'
        data={data}
        style={{color: 'black'}}
        onChangeText={(data) => {
          this.props.onChangeText(data)
        }}
       />
      </View>
        );
  }
}