import React, { Component } from "react";
import { Text, View, StyleSheet, TextInput } from "react-native";
import { Container, Header, Content, ListItem, Radio, Right, Left, Item, Input } from 'native-base';

export default class SelectGender extends Component {
  render() {
    return (
      <View style={styles.row}>
        <View style={{width: '20%'}}>
     
            <Item disabled>
              <Input disabled placeholder="+91 "
              iconName="ios-heart"
              iconColor="#f42725" />


             
            </Item>
        
        </View>

        <View style={{width: '80%'}}>
        <Item success>
              <Input
              keyboardType="phone-pad"
              style={{ borderBottomColor: 'red' }} placeholder="Enter your mobile number" />
            </Item>
         
       
        </View>
       
      </View>
    );
  }
}

const styles = StyleSheet.create({
  row: {
    flex: 1,
    flexDirection: "row"
  },
  inputWrap: {
    flex: 1,
    // borderColor: "#cccccc",
    // borderBottomWidth: 1,
    marginBottom: 10
  },
  inputdate: {
    fontSize: 14,
    marginBottom: -12,
    color: "#6a4595"
  },
  inputcvv: {
    fontSize: 14,
    marginBottom: -12,
    color: "#6a4595"
  }
});