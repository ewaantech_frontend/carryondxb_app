import React from 'react'
import * as yup from 'yup'
import { CustomButton } from '../ui'
import styled from 'styled-components/native'
import {  ScrollView } from 'react-native';  

const AboutText = styled.Text`
  font-family: 'SFProTextRegular';
  font-size: 15;
  color: #8a8a8f;
  margin: 0px 16px;
  line-height: 24;
`

const Hero = styled.Image.attrs({
  source: require('../../../assets/images/profile_hero.png'),
  resizeMode: 'cover'
})`
  width: 100%;
  height: 300;
  margin: 0px 0px 16px;
`

const profilePhotoDimensions = 100
const ProfilePhoto = styled.Image.attrs({
  source: require('../../../assets/images/home_page_new.png'),
})`
  width: ${profilePhotoDimensions};
  height: ${profilePhotoDimensions};
  border-radius: ${profilePhotoDimensions / 2};
  border-width: 5;
  border-color: #fff;
  align-self:flex-start;
  margin-top: 5;
`
const ButtonWrapper = styled.View`
  margin: 0px 6px;
`

export default function SignUpForm() {
  return (
   
    <ScrollView > 
       
         <Hero />

      <AboutText>
        Learning App gives you unlimited access to thousands of online classes.
        {`\n\n`}
        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum

        {`\n\n`}
        It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like
        
          {`\n\n`}
        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum

      </AboutText>
      <ButtonWrapper>
      <CustomButton  text={"NEXT"} onPressAction={() => props.handleSubmit()}/>
      </ButtonWrapper>
   
      </ScrollView>  
          
      
  )
}

