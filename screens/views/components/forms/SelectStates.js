import React from 'react'
import { View, Text, TouchableOpacity, Keyboard } from 'react-native'
import ModalFilterPicker from '../modal-picker'
import { FormField } from '../ui'
import { serviceWrapper } from '../../services/serviceWrapper';
import { cancellablePromise } from "../../services/cancellable-promise";
import { connect } from 'react-redux';

class SelectStates extends React.Component {
  constructor (props) {
    super(props);

    this.state = {
      visible: false,
      picked: this.props.value,
      error: this.props.error,
      options: [],
      mapped: {},
      pickedShow: ''
    };
    
  }
  
  pendingPromises = [];

  appendPendingPromise = promise =>
    this.pendingPromises = [...this.pendingPromises, promise];

  removePendingPromise = promise =>
    this.pendingPromises = this.pendingPromises.filter(p => p !== promise);

  componentDidMount() {
    this.pendingPromises.map(p => p.cancel());
    this.bindCountry();
  }

  bindCountry = () => {
      if(this.state.options.length > 0) {
          return true; // Map only once.
      }
    const obj = new serviceWrapper();
    const data = {
        "X-API-KEY": this.props.userInfo.key,
    }
    const wrappedPromise = cancellablePromise(obj.post('alms/state', data));
    this.appendPendingPromise(wrappedPromise);
    wrappedPromise.promise
      .then((res) => {
        this.setState({formLoading: false});
        if(res !== 'errors') {
          if(res.status === true) {
              let items = [];
              let mapVal = {};
              const states = res.state_list;
            for(var inc=0; inc < states.length; inc++) {
                items.push({
                    key: states[inc].id,
                    label: states[inc].state_name
                });
                mapVal[states[inc].id] = states[inc].state_name; // Just to display selected item.
            }
            this.setState({
                options: items,
                mapped: mapVal
            });
          }
        }
      })
      .then(() => this.removePendingPromise(wrappedPromise))
    .catch(errorInfo => {
      this.setState({formLoading: false});
      if (!errorInfo.isCanceled) {
        this.removePendingPromise(wrappedPromise);
      }
    });
  }

  render() {
    const { visible } = this.state;

    return (
      <View >
        <TouchableOpacity onPress={this.onShow}>
            <FormField
            label="Select State"
            value={ this.state.pickedShow }
            onChangeText={(text) => this.props.onChangeText(text) }
            error={this.state.error}
            onFocus={this.onShow}

            uniqueid={this.props.uniqueid}
            focusedfield={this.state.focusedfield}
        />
        </TouchableOpacity>
        <ModalFilterPicker
          visible={visible}
          onSelect={this.onSelect}
          onCancel={this.onCancel}
          options={this.state.options}
        />
      </View>
    );
  }

  onShow = (id) => {
    Keyboard.dismiss();
    this.props.onFocus(id);
    this.setState({ visible: true });
  }

  onSelect = (picked) => {
    this.setState({
      picked: picked,
      visible: false,
      error: '',
      pickedShow: this.state.mapped[picked]
    });
    this.props.onChangeText(picked);
  }

  onCancel = () => {
    this.setState({
      visible: false
    });
  }
}


const mapStateToProps = (state) => {
    // Redux Store --> Component
    return {
        userInfo: state.userReducer.userInfo,
        loggedIn: state.authReducer.loggedIn,
    };
  };
  
  export default connect(mapStateToProps)(SelectStates);