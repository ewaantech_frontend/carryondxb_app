import React from 'react'
import PropTypes from 'prop-types'
import Image from 'react-native-image-progress'
import styled from 'styled-components/native'

const ImageProgressWrapper = styled(Image).attrs({
  indicatorProps: {
    size: 30,
    thickness: 2,
    color: '#000',
    indeterminate: false
  }
})``

export default class ImageProgressComponent extends React.Component {
  render() {
  return (
    <ImageProgressWrapper
      style={this.props.style}
      source={{ uri: this.props.photoURL }}
      imageStyle={this.props.imageStyle}
      resizeMode={this.props.resizeMode}
    />
  )
  }
}

ImageProgressComponent.propTypes = {
  photoURL: PropTypes.string.isRequired,
  imageStyle: PropTypes.object,
  style: PropTypes.object.isRequired,
  resizeMode: PropTypes.string
}
ImageProgressComponent.defaultProps = {
  resizeMode: 'contain'
}
