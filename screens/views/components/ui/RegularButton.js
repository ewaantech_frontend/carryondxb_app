import React from 'react'
import PropTypes from 'prop-types'
import { Button } from 'native-base'
import styled from 'styled-components/native'

const Wrapper = styled(Button).attrs({
  full: true,
  rounded: true
})`
  height: 50;
  justify-content: center;
  align-items: center;
  margin: 25px 0px 10px;
  background-color: #E7E9E8;
`

const ButtonText = styled.Text.attrs({
  numberOfLines: 1
})`
  font-family: 'SFProTextBold';
  font-size: 15;
  color: #D0D2D1;
`

// export default function RegularButton({ text, onPress }) {
//   return (
//     <Wrapper onPress={() => onPress()}>
//       <ButtonText>{text}</ButtonText>
//     </Wrapper>
//   )
// }

export default function RegularButton({ text }) {
  return (
    <Wrapper>
      <ButtonText>{text}</ButtonText>
    </Wrapper>
  )
}

// RegularButton.propTypes = {
//   text: PropTypes.string.isRequired,
//   onPress: PropTypes.func.isRequired
// }

RegularButton.propTypes = {
  text: PropTypes.string.isRequired
}
