import React, { Component } from 'react';
import { Container, Header, Content, Card, CardItem, Text, Right, Item, Input,Button ,Body} from 'native-base';
import {
  StyleSheet,
  TouchableOpacity,
  Keyboard
} from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
 } from 'react-native-responsive-screen';
 import Icon from 'react-native-vector-icons/Ionicons';
export default class UpcomingSearch extends Component {

  constructor(props) {
    super(props);
    this.state = {
      searchTerm: ''
    }
  }

  render() {
    return (
          <Card style={[styles.Wrapper]} >
            <CardItem>
             <Item style={[styles.item]}>
            <Input value={ this.state.searchTerm }
              onSubmitEditing={() =>{console.debug('dismiss'); Keyboard.dismiss()}  }
              onChangeText={(text) => { this.setState({searchTerm: text}) }} 
              placeholderTextColor='#a7adbf'
              style={[styles.placeholderText]} placeholder="ENTER ORDER ID & SEARCH"/>
             </Item> 
             <TouchableOpacity style={[styles.searchbotton]} onPress={() => { Keyboard.dismiss(); this.props.onSubmit(this.state.searchTerm) } }>
              <Text style={[styles.SearchText]} >SEARCH</Text>
              </TouchableOpacity>
         
          <TouchableOpacity onPress={() => {  
            this.setState({searchTerm: ''});
            Keyboard.dismiss();
            this.props.onSubmit('');
          }}>
            <Icon style={[styles.icon]} name='md-refresh'size={25}  color="#1972b5"/> 
           </TouchableOpacity>
          </CardItem>
           </Card>
    );
  }
}

const styles = StyleSheet.create({
  Wrapper: {
    flexDirection: 'row',
    height:hp('14%'),
    width:wp('98%'),
    marginTop: 20,
    paddingRight:16,
    borderTopStartRadius: 10,
    borderTopEndRadius: 10
  },
  placeholderText: {
  //  color:'#a7adbf',
    fontSize: 12,
    fontFamily: 'Montserrat-Medium', 
   
  },
  SearchText: {
    fontSize: 13,
    fontFamily: 'Montserrat-Medium',
    textAlign: "center",
    marginLeft:7,
     color:'#ffffff',
     marginTop:8,
  },
  searchbotton:
  {
    backgroundColor: "#f25f1b" ,
    borderRadius:8,
    height:hp('5.5%'),
    width:wp('28%'),
    marginTop:15,
    fontFamily: 'Montserrat-Medium',
  },
  icon:
  {
   marginLeft:wp('3%'),
   marginTop:15,
  }
 , item: {
 
  width:wp('55%')
},
 
});
