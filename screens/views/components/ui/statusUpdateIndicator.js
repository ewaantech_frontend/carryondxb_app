import React from 'react'
import PropTypes from 'prop-types'
import Modal from 'react-native-modal'
import styled from 'styled-components/native'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

const Wrapper = styled.View`
  width: 95%;
  height: 50;
  justify-content: center;
  align-self: center;
  border-radius: 8;
  background-color: #f25f1b;
`

const Label = styled.Text`
  font-family: 'SFProTextRegular';
  font-size: 15;
  color: #fff;
  text-align: center;
  margin-top: 0;
`

export default function StatusUpdateIndicator({ text }) {
  return (

    <Modal isVisible={true} style={{ justifyContent: "flex-end", marginBottom: 36, flex: 1, }}>
      <Wrapper>
        <Label><Icon name={"check"} ></Icon> {text}</Label>
      </Wrapper>
    </Modal>
  )
}

StatusUpdateIndicator.propTypes = {
  text: PropTypes.string
}
StatusUpdateIndicator.defaultProps = {
  text: 'Updated'
}
