import React, { useEffect } from 'react';
import messaging from '@react-native-firebase/messaging';
import { StyleSheet, Image, Dimensions, AsyncStorage } from 'react-native';
import {
heightPercentageToDP as hp,
widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
const {
height: SCREEN_HEIGHT,
} = Dimensions.get('window');

const IS_IPHONE_X = SCREEN_HEIGHT === 812 || SCREEN_HEIGHT === 896;
const STATUS_BAR_HEIGHT = Platform.OS === 'ios' ? (IS_IPHONE_X ? 44 : 20) : 0;

function Notification() {
  console.debug('Notify messges');
  useEffect(() => {
    const unsubscribe = messaging().onMessage(async remoteMessage => {
      console.debug('FCM Message Data:', remoteMessage.data);
 
      // Update a users messages list using AsyncStorage
      const currentMessages = await AsyncStorage.getItem('messages');
      const messageArray = [];
      messageArray.push(remoteMessage.data);
      await AsyncStorage.setItem('messages', JSON.stringify(messageArray));
    });
  }, []);

  return (
    <Image     
        source={require('../../../images/group@x.png')}
        style={[styles.icon2]}
    />
  )
}

export default Notification;

const styles = StyleSheet.create({
    icon2:
  {
    top: 0,
    left:wp('6%'),
    color:"#434a5e",
    resizeMode: "contain",
    width:wp('4%'),
    height: hp('4.1%'),
    paddingTop: STATUS_BAR_HEIGHT,
  }
})