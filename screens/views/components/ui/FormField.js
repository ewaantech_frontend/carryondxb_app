import React from 'react'
import PropTypes from 'prop-types'
import { Item, Input } from 'native-base'
import styled from 'styled-components/native'


const Wrapper = styled.View`
margin: 8px 0px;
`
// margin: 8px 0px;
const FormFieldWrapper = styled(Item).attrs({
  rounded: true
})`
  
  borderRadius: 0;
  borderBottomColor:#747474;
  borderTopColor:white;
  borderEndColor:white;
  borderStartColor:white;
  padding: 0px 8px;
  margin: 0px;
  borderBottomWidth:1;
  
`

const TextBox = styled(Input)`
  font-family: 'SFProTextRegular';
  font-size: 8;
  color: ${props => (props.disabled === true ? '#a5a5a5' : '#000')};
`

const Error = styled.Text`
  font-family: 'SFProTextRegular';
  font-size: 12;
  color: #b00727;
  margin: 4px 8px;
`

let touchedArray = [];

export default function FormField({
  label,
  value,
  error,
  secure,
  disabled,
  keyboardType,
  returnKeyType,
  onChangeText,
  onSubmitEditing,
  onFocus,
  uniqueid,
  focusedfield
}) {
  if(typeof onFocus != 'undefined') {
    if (focusedfield == uniqueid) {
      touchedArray.push(uniqueid);
    }

    let wrapperStyle;
    if(error && touchedArray.includes(uniqueid)) {
      if (focusedfield == uniqueid) {
        color = '#12c20c';
        wrapperStyle = {borderBottomWidth: 2,fontSize: 15, borderBottomColor: color};
      } else {
        color = '#747474';
      }
      if (color == '#747474') {
        color = '#b00727';
        wrapperStyle = {fontSize: 15, borderBottomColor: color};
      }
    } else {
      error = false;
      if (focusedfield == uniqueid) {
        color = '#12c20c';
        wrapperStyle = {borderBottomWidth: 2,fontSize: 15, borderBottomColor: color};
      } else {
        color = '#747474';
        wrapperStyle = {fontSize: 15, borderBottomColor: color};
      }
    } 
      return (
        <Wrapper>
          <FormFieldWrapper style={wrapperStyle}>
            <TextBox
              disabled={disabled}
              placeholder={label}
              placeholderTextColor="#a5a5a5"
              value={value}
              secureTextEntry={secure}
              keyboardType={keyboardType}
              returnKeyType={returnKeyType}
              onChangeText={text => onChangeText(text)}
              onSubmitEditing={() => onSubmitEditing()}
              onFocus={() => onFocus(uniqueid) }
              style={{fontSize: 15, borderBottomColor: color}}
              underlineColorAndroid="transparent"
              placeholderTextSize = {{fontSize: 15}}
  
            />
  
  
          </FormFieldWrapper>
  
          {!!error && <Error>{error}</Error>}
        </Wrapper>
      )
  } else {
    let wrapperStyle;
    if(error) {
      color = '#b00727';
      wrapperStyle = {fontSize: 15, borderBottomColor: color};
    } else {
      color = '#747474';
      wrapperStyle = {fontSize: 15, borderBottomColor: color};
    }
      return (
        <Wrapper>
          <FormFieldWrapper  style={wrapperStyle}>
            <TextBox
              disabled={disabled}
              placeholder={label}
              placeholderTextColor="#a5a5a5"
              value={value}
              textAlign={'left'}
              secureTextEntry={secure}
              keyboardType={keyboardType}
              returnKeyType={returnKeyType}
              onChangeText={text => onChangeText(text)}
              onSubmitEditing={() => onSubmitEditing()}
              style={{fontSize: 15}}
              underlineColorAndroid="transparent"
              placeholderTextSize = {{fontSize: 15}}
  
            />
          </FormFieldWrapper>
  
          {!!error && <Error>{error}</Error>}
        </Wrapper>
      )
  }
}

FormField.propTypes = {
  // label: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
  error: PropTypes.string,
  secure: PropTypes.bool,
  disabled: PropTypes.bool,
  keyboardType: PropTypes.string,
  returnKeyType: PropTypes.string,
  onChangeText: PropTypes.func.isRequired,
  onSubmitEditing: PropTypes.func
}
FormField.defaultProps = {
  secure: false,
  disabled: false,
  keyboardType: 'default',
  returnKeyType: 'next',
  error: '',
  onSubmitEditing: () => null
}
