import React, { Component } from 'react';
import { Container, Header, Content, Card, CardItem, Text, Right, Item, Input,Button ,Body,DatePicker} from 'native-base';
import {
  StyleSheet,
  Platform,
  Animated,
  View,
  Dimensions,
  StatusBar,
  FlatList,
} from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
 } from 'react-native-responsive-screen';
 import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
 //import DatePickerDxb from './DatePickerDxb'

export default class SearchwithCalender extends Component {
    constructor(props) {
        super(props);
        this.state = { chosenDate: new Date() };
        this.setDate = this.setDate.bind(this);
      }
      setDate(newDate) {
        this.setState({ chosenDate: newDate });
        this.state.chosenDate.toString().substr(4, 12);
      }
  render() {
   
    return (
       
          <Card style={[styles.Wrapper]}>
                <CardItem>
                <Item>
               <DatePicker
            minimumDate={new Date(2020, 1, 1)}
            maximumDate={new Date(2025, 12, 31)}
            locale={"en"}
            timeZoneOffsetInMinutes={undefined}
            modalTransparent={false}
            animationType={"fade"}
            androidMode={"default"}
            placeHolderText="Select from date"
            textStyle={{ color: "green" }}
            placeHolderTextStyle={{ color: "#a7adbf", fontFamily: 'Montserrat-Medium' , fontSize: 12,}}
            onDateChange={this.setDate}
            disabled={false}
            />
            <Icon active name='calendar-month-outline' size={25}  color="#a7adbf"/>
              </Item>
              <Item style={[styles.calendar]}>
               <DatePicker
            minimumDate={new Date(2020, 1, 1)}
            maximumDate={new Date(2025, 12, 31)}
            locale={"en"}
            timeZoneOffsetInMinutes={undefined}
            modalTransparent={false}
            animationType={"fade"}
            androidMode={"default"}
            placeHolderText="Select to date"
            textStyle={{ color: "green" }}
            placeHolderTextStyle={{ color: "#a7adbf", fontFamily: 'Montserrat-Medium', fontSize: 12, }}
            onDateChange={this.setDate}
            disabled={false}
            />
            <Icon active name='calendar-month-outline' size={25}  color="#a7adbf"/>
            </Item>
            </CardItem>
            <CardItem>
             <Item style={[styles.item]} >
            <Input style={[styles.placeholderText]}  placeholderTextColor='#a7adbf' placeholder="ENTER ORDER ID & SEARCH"/>
            </Item>
            <Button style={[styles.searchbotton]}>
              <Text  style={[styles.SearchText]} >SEARCH</Text>
            </Button>
           <Icon style={[styles.icon]} name='refresh'size={25}  color="#1972b5"/> 
          </CardItem>
           </Card>
        
    );
  }
}

const styles = StyleSheet.create({
  Wrapper: {
      flex:1,
    //flexDirection: 'row',
    height:hp('15%'),
    width:wp('100%'),
    marginTop: 20,
    paddingRight:16,
    borderTopStartRadius: 10,
    borderTopEndRadius: 10
  },
  placeholderText: {
    //color:'#a7adbf',
  
    fontSize: 12,
    fontFamily: 'Montserrat-Medium', 
    // width:wp('20%')
  },
 item: {
  
    width:wp('56%')
  },
  SearchText: {
    fontSize: 13,
    fontFamily: 'Montserrat-Medium',
  },
  searchbotton:
  {
    backgroundColor: "#f25f1b" ,
    borderRadius:8,
    height:40,
    width:90,
    //margin:8,
    marginTop:8,
    fontFamily: 'Montserrat-Medium',
  },
  icon:
  {
    marginRight:5,
   
  },
  calendar:
  {
    marginLeft:35,
   
  }
 
});
