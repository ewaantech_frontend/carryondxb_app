import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components/native'

const Price = styled.Text`
  font-family: 'ArialRoundedMTBold';
  font-size: 13;
  color: #cc2500;
  text-align: right;
  marginBottom:5;
`

export default function CoursePriceTag({price}) {
  return (
      <Price>{price}</Price>
  )
}

CoursePriceTag.propTypes = {
  price: PropTypes.string.isRequired
}
