import React from 'react'
import Icon from 'react-native-vector-icons/Ionicons';
import { StyleSheet, TouchableOpacity, Alert, View, Animated } from 'react-native';
import { connect } from 'react-redux';
import {
    heightPercentageToDP as hp,
    widthPercentageToDP as wp,
   } from 'react-native-responsive-screen';
const AnimatedTouchable = Animated.createAnimatedComponent(TouchableOpacity);

class Logout extends React.Component {

    constructor(props) {
        super(props)
    }

    logout = () => {
        Alert.alert(
            'Logout',
            'Are you sure to logout?',
            [
              {
                text: 'Cancel',
                onPress: () => null,
                style: 'cancel',
              },
              {text: 'OK', onPress: () => this.logoutConfirm() },
            ],
            {cancelable: false},
          );
    }

    logoutConfirm = () => {
        const user = {};
        this.props.reduxLogin(false);
        this.props.reduxStoreUser(user);
        this.props.navigation.navigate('Login');
    }

    render() {
        return (
            <AnimatedTouchable onPress={() => {
                this.logout
            }} >
                <Icon style={[styles.icon1, { paddingTop: this.props.status_bar_height }]} name="ios-log-out"  size={25} />
            </AnimatedTouchable>
        )
    }

}

const mapStateToProps = (state) => {
    return {
        userInfo: state.userReducer.userInfo,
        loggedIn: state.authReducer.loggedIn,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        reduxStoreUser: (payload) => dispatch({
            type: 'STORE_USER',
            payload: payload,
        }),
        reduxLogin: (payload) => dispatch({
            type: 'LOGGED_IN',
            payload: payload,
        }),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Logout);

const styles = StyleSheet.create({

    icon1:
    {
      top: 0,
      left: wp('5%'),
      color:"#434a5e",
    }

});
