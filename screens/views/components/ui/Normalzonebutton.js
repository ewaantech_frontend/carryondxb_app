
import React, { Component } from 'react';
import { StyleSheet, View, Text, Image, TouchableOpacity } from 'react-native';
 
export default class Normalzonebutton extends Component {
  render() {
    return (
      <View style={styles.MainContainer}>
        <TouchableOpacity style={styles.NormalStyle} activeOpacity={0.5} onPress={() => this.props.onPress() }>
          <Image
            source={require('../../../assets/images/normal.png')}
            style={styles.ImageIconStyle}
          />
          <View style={styles.SeparatorLine} />
          <Text style={styles.TextStyle}> NORMAL ZONE </Text>
        </TouchableOpacity>
      </View>
    );
  }
}
 
const styles = StyleSheet.create({
  MainContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    margin: 10,
  },
  NormalStyle: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    backgroundColor: '#2EBC8F',
    borderWidth: 0.5,
    borderColor: '#fff',
    height: 60,
    width: 250,
    borderRadius: 15,
    marginTop: -100,
  },
  
  ImageIconStyle: {
    padding: 10,
    margin: 5,
    height: 50,
    width: 50,
    resizeMode: 'stretch',
  },
  TextStyle: {
    alignItems: 'center',
    color: '#fff',
    marginBottom: 4,
    marginTop: 15,
    fontSize: 20,
    fontFamily:"ArialRoundedMTBold"

  },
  SeparatorLine: {
    backgroundColor: '#fff',
    width: 1,
    height: 80,
  },
});