import React from 'react'
import PropTypes from 'prop-types'
import {Icon as Ionicons } from "react-native-vector-icons/Ionicons";
import styled from 'styled-components/native'

export const Wrapper = styled.View`
  flex: 1;
  padding: 16px;
  justify-content: center;
  align-items: center;
`

const iconWrapperDims = 80
export const IconWrapper = styled.TouchableOpacity`
  width: ${iconWrapperDims};
  height: ${iconWrapperDims};
  justify-content: center;
  align-items: center;
  border-radius: ${iconWrapperDims / 2};
  background-color: #f5fcf3;
`

export const Icon = styled(Ionicons).attrs({
  size: 30,
  color: '#84d5b1'
})``

export const Title = styled.Text.attrs({
  numberOfLines: 1
})`
  font-family: 'SFProTextRegular';
  font-size: 15;
  margin-top: 8;
`

export default function CourseCategoryGridItem({ icon, title, onPress }) {
  return (
    <Wrapper>
      <IconWrapper onPress={() => onPress()}>
        <Icon name={icon} />
      </IconWrapper>
      <Title>{title}</Title>
    </Wrapper>
  )
}

CourseCategoryGridItem.propTypes = {
  icon: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  onPress: PropTypes.func.isRequired
}
