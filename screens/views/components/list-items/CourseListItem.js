import React from 'react'
import PropTypes from 'prop-types'
import { Card, Left, Badge } from 'native-base'
import styled from 'styled-components/native'
import { CoursePriceTag } from '../ui'
// import { BASE_URL } from '../../services/serviceWrapper'
import { Text } from 'react-native'

const Wrapper = styled(Card)`
  flex-direction: row;
  height: 150;
  padding: 16px;
  margin-top: 16;
  border-top-color: #f4f6fa;
  border-radius: 8;
`

export const Right = styled.View`
  flex: 1;
  margin-left: 16;
`

export const Top = styled.View`
  flex-direction: row;
`

export const DetailsWrapper = styled.TouchableOpacity`
  flex: 1;
`

export const Title = styled.Text.attrs({
  numberOfLines: 1
})`
  font-family: 'SFProTextRegular';
  font-size: 17;
  margin-right: 8;
`

export const SubHeading = styled.Text`
  font-family: 'SFProTextRegular';
  font-size: 12;
  color: #8a8a8f;
  margin-top: 4;
`

const MetaWrapper = styled.View`
  flex-direction: row;
  margin-top: 16;
  border-color: transparent;
  border-top-color: #dee3ea;
  border-width: 1;
`

const CourseMetadataWrapper = styled.View`
  flex: 1;
  margin-top: 8;
`

const CoursePriceWrapper = styled.View`
  flex: 1;
  justify-content: flex-end;
`

export default class CourseListItem extends React.Component {
render() {
  const increment = typeof this.props.increment != 'undefined' ? this.props.increment : '';
  return (
    <Wrapper > 
          <Badge warning style={{ marginTop: 7}}>
            <Text style={{marginTop:3}}>{ increment+1 }</Text>
          </Badge>
      
      <Right>
        <Top>
          <DetailsWrapper>
            <SubHeading>{this.props.qty}</SubHeading>
            <SubHeading>{this.props.price}</SubHeading>
          </DetailsWrapper>
        </Top>
        <MetaWrapper>
          <CourseMetadataWrapper>
             {/* <Title></Title>  */}
            <SubHeading></SubHeading>
          </CourseMetadataWrapper> 
          <CourseMetadataWrapper>
            <CoursePriceWrapper>
            <CoursePriceTag price={this.props.price} />
            </CoursePriceWrapper>
          </CourseMetadataWrapper>
        </MetaWrapper>
      </Right>
    </Wrapper>
  )
}
}

CourseListItem.propTypes = {
  photoURL: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  mobile: PropTypes.string.isRequired,
  Email: PropTypes.string.isRequired,
  price: PropTypes.string.isRequired,
  Date: PropTypes.string.isRequired,
   allowSave: PropTypes.bool,
   onPress: PropTypes.func.isRequired,
}
