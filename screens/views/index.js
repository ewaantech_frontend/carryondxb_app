import AllOrders from './allOrders/AllOrders'
import Dashboard from './dashboard/Dashborad'
import Login from './login/Login'
import Notification from './notification/Notification'
import Passwordresetmessage from './Passwordresetmessage/Passwordresetmessage'
import Splash from './splash/Splash'

import UpcomingnewScreen from './upcoming/UpcomingnewScreen'

import Updateprofile from './updateprofile/updateprofile'

import PickupMainScreen from './ReadyForPickup/PickupMainScreen'

import OrderDetials from './orderdetails/Orderdetails'
import table from './orderdetails/table'

export default {
  // A--------------------
        AllOrders,
  // B--------------------
  
  // C--------------------
  
  // D--------------------
      Dashboard,
  // E--------------------
  
  // F--------------------
  
  // H--------------------
  
  // I--------------------
  
  // L--------------------
      Login,
  // M--------------------

  // N---------------------
      Notification,
  // O--------------------
  OrderDetials,
  // P--------------------
     Passwordresetmessage,
     PickupMainScreen,
      //  PickupListScreen,
  // R--------------------
  
  // S--------------------
      Splash,
  // T--------------------
      table,
  // U--------------------
      //   Upcoming, 
      UpcomingnewScreen,
      //   UpcomingListnew,
      Updateprofile
  // W--------------------
  
  // Z--------------------
  
}
