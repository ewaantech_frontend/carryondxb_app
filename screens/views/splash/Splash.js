import React, { Component } from "react";
import { StyleSheet, ImageBackground, View, Image } from "react-native";
import { connect } from 'react-redux';

class Splash extends Component {
  static navigationOptions = {
    headerShown: false
  }
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    const that = this;
    setTimeout(function(){
      if(that.props.loggedIn == 'LOGGEDIN') {
        that.props.navigation.navigate('Upcoming');
        return true;
      }
      that.props.navigation.navigate('Login');
    },1000);
  }

  render() {
    return (
      <View style={{backgroundColor: "#D35211", height: "100%"}}>
      <ImageBackground
        style={{ flex: 1, opacity:1 }}
         source={require('../../images/splash.png')}
        >
          
            <Image
                source={require('../../images/logo.png')}
                style={{ width: 200, height: 60, marginTop: "80%", resizeMode: "contain",alignSelf: "center" }}
            />
          
      </ImageBackground>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  return {
      userInfo: state.userReducer.userInfo,
      loggedIn: state.authReducer.loggedIn,
  };
};

export default connect(mapStateToProps)(Splash);

const styles = StyleSheet.create({

  row: {
    flex: 1,
    flexDirection: 'row',
  },

});