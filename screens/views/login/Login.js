import React from 'react';
import { View, Text, StyleSheet, Image, KeyboardAvoidingView,
        TouchableOpacity, ToastAndroid, Keyboard ,StatusBar,} from 'react-native';
import { heightPercentageToDP as hp, widthPercentageToDP as wp, } from 'react-native-responsive-screen';
import { Item, Input, Label,Button, } from 'native-base';
import { Container, Header, Content, Card, CardItem,  Body } from 'native-base';
import Icon from 'react-native-vector-icons/Ionicons';
import { serviceWrapper } from '../../services/serviceWrapper';
import { cancellablePromise } from "../../services/cancellable-promise";
import { connect } from 'react-redux';
import { CustomActivityIndicator } from '../components/ui'
import { firebase } from '@react-native-firebase/messaging';

class Login extends React.Component {

    constructor(props) {
      super(props);
      this.state = {
        icon: "eye-off",
        password: true,
        userEmail: '',
        userPassword: '',
        passColor: '#F0EDEC',
        emailColor: '#F0EDEC',
        loading: false,
        fcmAPNS_token: ''
      };
    }

    static navigationOptions = {
      headerShown: false
    }

    fcmAPNS_token = (async() => {
      try{
        const fcmToken = await firebase.messaging().getToken();
        if (fcmToken && this.state.fcmAPNS_token == '') {
          this.setState({ fcmAPNS_token: fcmToken });
        }
      } catch(error) {
        console.debug(error)
      }
    });

    pendingPromises = [];

    appendPendingPromise = promise =>
      this.pendingPromises = [...this.pendingPromises, promise];

    removePendingPromise = promise =>
      this.pendingPromises = this.pendingPromises.filter(p => p !== promise);

    componentDidMount() {
      this.pendingPromises.map(p => p.cancel());
      this.fcmAPNS_token();
    }

    _changeIcon() {
        this.setState(prevState => ({
            icon: prevState.icon === 'md-eye' ? 'md-eye-off' : 'md-eye',
            password: !prevState.password
        }));
    }

    login = () => {
        let flag = true;
        if( this.state.userEmail == '') {
          this.setState({ emailColor: 'red'});
          flag = false;
        }
        if( this.state.userPassword == '') {
          this.setState({ passColor: 'red'});
          flag = false;
        }
        if( flag == true) {
          this.setState({ loading: true});
          const obj = new serviceWrapper();
          const data = {
            username: this.state.userEmail,
            password: this.state.userPassword,
            fcm_token: this.state.fcmAPNS_token
          }
          console.debug(data)
          const wrappedPromise = cancellablePromise(obj.post('login', data));
          this.appendPendingPromise(wrappedPromise);
          wrappedPromise.promise
            .then((res) => {
              this.setState({ loading: false});
              if(res !== 'errors') {
                if(res.status === true) {
                  const user = {
                    key: res.key,
                    name: res.user_data.name,
                    phone_number: res.user_data.phone_number,
                    email: res.user_data.email,
                    user_id: res.user_data.id,
                    is_defalut_user: res.user_data.is_default_pickup_person,
                    pickup_location: res.user_data.pickup_point
                  }
                  this.props.reduxLogin('LOGGEDIN');
                  this.props.reduxStoreUser(user);
                  ToastAndroid.showWithGravityAndOffset(
                    'You are logged in',
                    ToastAndroid.LONG,
                    ToastAndroid.BOTTOM,
                    25,
                    50,
                  );
                  this.props.navigation.navigate('Upcoming');
                } else {
                  ToastAndroid.showWithGravityAndOffset(
                    res.message,
                    ToastAndroid.LONG,
                    ToastAndroid.BOTTOM,
                    25,
                    50,
                  );
                }
              }
            })
            .then(() => this.removePendingPromise(wrappedPromise))
          .catch(errorInfo => {
            this.setState({formLoading: false});
            if (!errorInfo.isCanceled) {
              this.removePendingPromise(wrappedPromise);
            }
          });
        }
    }

    render() {
        return (
            <KeyboardAvoidingView style={{ flex: 1 }} behavior="position">
 
                    <View style={styles.MainContainer}>
                    <StatusBar backgroundColor="#f25f1b" barStyle="light-content"
   />
                    <Image
                        source={require('../../images/logo.png')}
                        style={{ width: 200, height: 60, marginTop: 90, resizeMode: "contain" }}
                    />
                    </View>
        
                   
          <Card style={styles.cardstyle}>
            <CardItem header>
              <Text style={styles. headertext}>Proceed with your</Text>
            </CardItem>
            <Text style={styles. headertext2}>Login</Text>
            <CardItem style={{marginTop: 10}}>
              <Body>
                
                           <Item >
                                {/* <Label style={{  color: '#a7adbf', fontSize: 14,fontFamily:'Montserrat-Regular',}}>Emaill</Label> */}
                                <Input style={{ borderBottomColor: this.state.emailColor, borderBottomWidth:1 ,fontSize: 14,fontFamily:'Montserrat-Regular',width:wp('90%')}} 
                                placeholder='Email' placeholderTextColor='#a7adbf' onChangeText={(txt) => this.setState({ userEmail: txt, emailColor: '#F0EDEC' }) } autoCorrect={true} />
                            </Item>

                            <Item >

                                {/* <Label style={{  color: '#a7adbf', fontSize: 14,fontFamily:'Montserrat-Regular',}}>Password</Label> */}
                            

                                <Input style={{ borderBottomColor: this.state.passColor, fontSize: 14,  fontFamily: 'Montserrat-Regular',borderBottomWidth:1 ,width:wp('90%'),}} 
                                placeholder='password' placeholderTextColor='#a7adbf'
                                onChangeText={(txt) => this.setState({ userPassword: txt, passColor: '#F0EDEC' }) } secureTextEntry={this.state.password} 
                                />
                              <Icon   style={[styles.icon]} name='md-eye'size={15}  color="#a7adbf"/>
                            </Item>

                            {/* <TouchableOpacity style={{ borderColor: 'transparent', }}>
                                <Text style={{ marginTop: 20, color: '#a7adbf', fontSize: 14,fontFamily:'Montserrat-Regular', }}>Forgot your Password ?</Text>
                                </TouchableOpacity> */}

                            <TouchableOpacity onPress={() => { Keyboard.dismiss(); this.login() }} style={{
                                  backgroundColor: "#ff8f5b",
                                  width: wp('75%'),
                                  height: hp('9%'),
                                  marginTop: 80
                              }} >
                              <Text style={{
                                  color: "#fff", alignSelf: "center",
                                  fontSize: 20,
                                  marginTop: 15,
                                  fontFamily:'Montserrat-Regular'
                                  }}>LOGIN</Text>
                            </TouchableOpacity>
              </Body>
            </CardItem>
            </Card>
          
                    {this.state.loading && <CustomActivityIndicator />}

            </KeyboardAvoidingView>
        );
    }
}

const mapStateToProps = (state) => {
  // Redux Store --> Component
  return {
      userInfo: state.userReducer.userInfo,
      loggedIn: state.authReducer.loggedIn,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
      reduxStoreUser: (payload) => dispatch({
          type: 'STORE_USER',
          payload: payload,
      }),
      reduxLogin: (payload) => dispatch({
          type: 'LOGGED_IN',
          payload: payload,
      }),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);

const styles = StyleSheet.create({
    MainContainer: {
       // flex: 1,
        alignItems: 'center',
        height:  hp('40%'),
        backgroundColor: "#f25f1b"
    },
    TextStyle: {
        color: '#0250a3',
        textAlign: 'center',
        fontSize: hp('4.2%'),
        marginTop: 10,
    },
    cardstyle:
    {
       
      width: wp('90%'),
      marginTop:-55,
      padding:12,
      marginLeft:"5%",
     
    },
    bottomView: {
       
        width: wp('95%'),
        height: hp('40%'),
        backgroundColor: '#FFFFFF',
        justifyContent: 'center',
        alignItems: 'center',
        bottom: 0,
        shadowColor: "red",
        shadowOpacity: 0.8,
        shadowRadius: 2,
        shadowOffset: {
            height: 1,
            width: 0
        },
        // borderWidth:1,
        borderColor: "grey",
        marginLeft:"2.5%",
        marginRight:"2.5%",
        marginTop:-45,
    },
    textStyle: {
        color: '#fff',
        fontSize: 22
    },
    headertext: {
      color: '#a7adbf',
      fontSize: 18,
      fontFamily:'Montserrat-Regular',
      //margin:"0%",
    
  },
  headertext2: {
    color: '#434a5e',
    fontSize: 22,
    fontFamily:'Montserrat-Bold',
    marginLeft:"5%",
  //  marginTop:"0.2%",
  
},icon:
{
 // Color: "#1972b5" ,
  // height:hp('2%'),
  //marginRight:200,
   //marginBottom:"5%",
  // marginLeft:"80%",
  // flexDirection: "row",
  marginTop:"12%",
}

});
