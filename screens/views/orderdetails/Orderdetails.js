import React, { Component } from 'react';
import { Card,View, Content, Button, } from 'native-base'
import { ScrollView,Text,Image} from 'react-native';
import styled from 'styled-components/native'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { DataTable } from 'react-native-paper';
import { serviceWrapper } from '../../services/serviceWrapper';
import { cancellablePromise } from "../../services/cancellable-promise";
import { connect } from 'react-redux';
import { ListLoadingComponent, StatusUpdateIndicator } from '../components/ui'
import moment from 'moment';
import { FlatList, ToastAndroid } from 'react-native';
import UpdatePlModal from '../modals/UpdatePlModal'
import UpdateStatusModal from '../modals/UpdateStatusModal'
import { heightPercentageToDP } from 'react-native-responsive-screen';
import { NativeEventEmitter, NativeModules } from 'react-native';

var printerCustom = NativeModules.PrinterCustom;

const Wrapper = styled(View)`
  flex-direction: row;
  height:270;
  padding: 8px;
  margin-top: 16;
  border-top-color: #f4f6fa;
`

export const Right = styled.View`
  flex: 1;
  margin-left: 8;
`

export const Top = styled.View`
  flex-direction: row;
`

export const DetailsWrapper = styled.TouchableOpacity`
  flex: 1;
`

export const Title = styled.Text.attrs({
  numberOfLines: 1
})`
  font-family: 'Montserrat-Medium';
  font-size: 17;
  margin-right: 8;
  color:#434a5e ;
`

export const SubHeading = styled.Text`
  font-family: 'Montserrat-Regular';
  font-size: 15;
  color:#a7adbf  ;
  margin-top: 4;
`
export const PickupHeading = styled.Text`
  font-family: 'Montserrat-Regular';
  font-size: 12;
  margin-top: 4;
  color:#a7adbf;
  margin-Right:50;
`
export const StatusHeading = styled.Text`
  font-family: 'Montserrat-Regular';
  font-size: 12;
  margin-Right: 60;
  margin-top: 4;
  color:#a7adbf;
`
export const Statustext = styled.Text`
  font-family: 'SFProTextRegular';
  font-size: 15;
  margin-left: 60;
  margin-top: 2;
  color:#a7adbf;
`
export const PickupSubHeading = styled.Text`
  font-family: 'Montserrat-Regular';
  font-size: 15;
  color: #ffffff;
  margin-top: 10;
  margin-left: 30;
  
 
`
export const StatusSubHeading = styled.Text`
font-family: 'Montserrat-Medium';
  font-size: 15;
  color: #ffffff;
  margin-top: 10;
  text-align: center;
`
export const Buttontext = styled.Text`
  font-family: 'SFProTextRegular';
  font-size: 15;
  color: #1972b5;
`
export const Bottom = styled.View`
  flex-direction: row;
  margin-top: 16;
`
const MetaWrapper = styled.View`
  margin-top: 2;
  border-color: transparent;
  border-top-color: #dee3ea;
  border-width: 1;
`

const CourseMetadataWrapper = styled.View`
  flex: 1;
`
export const ButtonWrapper = styled.TouchableOpacity`
background: #0059b3;
border-radius:2;
margin-top: 8;
height:30%;
color:#1972b5;
`
export const StatusButtonWrapper = styled.TouchableOpacity`
border-radius:2;
margin-top: 8;
margin-left:12;
height:30%;
width:95%
color:#61b4ff;
`
export const PrintDetailsWrapper = styled.View`
font-family: 'SFProTextRegular';
font-size: 15;
margin-left: 3;
margin-top: 8;
`
export const ViewDetailsTextWrapper = styled.View`
font-family: 'SFProTextRegular';
font-size: 15;
margin-left: 80;
margin-top: 2;
`
export const Last = styled.View`
  flex-direction: row;
  margin-top: 10;
`
export const Pickup = styled.View`

`

class OrderDetials extends Component{

  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      order_data: {},
      encOrderId: this.props.navigation.getParam('encOrderId',''),
      scanOrderId: this.props.navigation.getParam('orderId',''),
      pickup_points: this.props.navigation.getParam('pickup_points', []),
      pickup_locations: this.props.navigation.getParam('pickup_locations', []),
      updatedScan:  this.props.navigation.getParam('update', ''),
      isModalVisible: false,
      isModalshow: false,
      statusUpdated: false
    };
  }

  pendingPromises = [];

    appendPendingPromise = promise =>
      this.pendingPromises = [...this.pendingPromises, promise];

    removePendingPromise = promise =>
      this.pendingPromises = this.pendingPromises.filter(p => p !== promise);

    componentDidMount() {
      this.pendingPromises.map(p => p.cancel());
      this.loadDetails();
      if( this.state.updatedScan ) {
        this.updateStatus();
      }

      // printerCustom.scanCode( (err) => {console.log(err)}, (msg) => {console.log(msg)} );

      // const eventEmitter = new NativeEventEmitter(NativeModules.ToastExample);
      // eventEmitter.addListener('barcode', (event) => {
      //   const order_id = parseInt( event.replace('CON', '') );
      //   this.setState( { scanOrderId: order_id });
      //   this.loadDetails();
      //   this.updateStatus();
      // });
    }

    updateStatus = () => {
      this.setState({ loading: true});
      const obj = new serviceWrapper();
      const location = this.props.userInfo.is_defalut_user == 'YES' ? 'Grabbit' : this.props.userInfo.pickup_location;
      let type = 'DESTINATION';
      const data = {
          text: location,
          order_status: type,
          order_id: this.state.scanOrderId,
          key: this.props.userInfo.key
      }
      const wrappedPromise = cancellablePromise(obj.post('change_status', data));
      this.appendPendingPromise(wrappedPromise);
      wrappedPromise.promise
          .then((res) => {
          this.setState({ loading: false});
          if(res !== 'errors') {
            console.debug(res)
              if(res.status === true) {
                  this.setState( { statusUpdated: true });
                  setTimeout(() => {
                    this.setState( { statusUpdated: false });
                  }, 2000);
                  this.loadDetails();
              } else {
                  ToastAndroid.showWithGravityAndOffset(
                      res.message,
                      ToastAndroid.LONG,
                      ToastAndroid.BOTTOM,
                      25,
                      50,
                  );
              }
          }
      })
      .then(() => this.removePendingPromise(wrappedPromise))
      .catch(errorInfo => {
          this.setState({formLoading: false});
          if (!errorInfo.isCanceled) {
          this.removePendingPromise(wrappedPromise);
          }
      });
  }

    loadDetails = () => {
          this.setState({ loading: true});
          const obj = new serviceWrapper();
          const data = {
            key: this.props.userInfo.key,
            order_id: this.state.scanOrderId
          }
          const wrappedPromise = cancellablePromise(obj.post('details', data));
          this.appendPendingPromise(wrappedPromise);
          wrappedPromise.promise
            .then((res) => {
              this.setState({ loading: false});
              if(res !== 'errors') {
                if(res.status === true) {
                  this.setState( { order_data: res, pickup_points: res.pickup_points, pickup_locations: res.pickup_points});
                } else {
                  ToastAndroid.showWithGravityAndOffset(
                    res.message,
                    ToastAndroid.LONG,
                    ToastAndroid.BOTTOM,
                    25,
                    50,
                  );
                }
              }
            })
            .then(() => this.removePendingPromise(wrappedPromise))
          .catch(errorInfo => {
            this.setState({formLoading: false});
            if (!errorInfo.isCanceled) {
              this.removePendingPromise(wrappedPromise);
            }
          });
    }

    renderOrderDetails = (item, index) => {
      
      return (

          <DataTable.Row >
              <DataTable.Cell style={{ flex:1 }}>{ index + 1 }</DataTable.Cell>
              <DataTable.Cell style={{ flex:2 }}>{ item.name  }</DataTable.Cell> 
              <DataTable.Cell numeric style={{ flex:1 }}>{ item.item_count }</DataTable.Cell>
              <DataTable.Cell numeric style={{ flex:1 }}>{ item.grant_total }</DataTable.Cell>
            </DataTable.Row>
      );
    }
  toggleModal = () => {
    this.setState({ isModalVisible: !this.state.isModalVisible });
  };
  toggleModalsecond = () => {
    this.setState({ isModalshow: !this.state.isModalshow });
  };

  render() {

    if( Object.keys( this.state.order_data ).length == 0) {
      return (
        <ListLoadingComponent text="Loading Orders" />
      )
    }

    const { loading, pickup_locations, pickup_points } = this.state;
    const customer_data = this.state.order_data.customer_details;
    const order_data = this.state.order_data.orders[0];
    const order_items = this.state.order_data.order_items;
    const date = moment( order_data.pickup_time )
                .utcOffset('+04:00')
                .format('MMM DD HH.mm A.');
    
    let status = order_data.status.toLowerCase().split("_").join(" ")
                    .split(' ')
                    .map((s) => s.charAt(0).toUpperCase() + s.substring(1))
                    .join(' ');
    if(order_data.status == 'READY_FOR_PICKUP' && order_data.pickup_reached_at != '') {
      status = order_data.pickup_reached_at;
    }
    let color = "#80bfff";
    if( status == 'In Kitchen' ) {
      color = "orange";
    }
    return (
      <Wrapper onPress={() => onPress()}>
        <Right>
          <Top>
              <CourseMetadataWrapper>
              <SubHeading>Order ID</SubHeading>
              <Title>{ this.state.encOrderId }</Title>
              </CourseMetadataWrapper>
              <CourseMetadataWrapper>
              <SubHeading>Flight</SubHeading>
              <Title>{ order_data.flight_number }</Title>
              </CourseMetadataWrapper>
              <CourseMetadataWrapper>
              <SubHeading>Pickup time</SubHeading>
              <Title>{ date }</Title>
              </CourseMetadataWrapper>
          </Top>
          <Bottom>
            <CourseMetadataWrapper>
              <Pickup >
                <Button iconLeft light transparent >
                <Image
                        source={require('../../images/pin.png')}
                         style={{ width: 20, height: 20, marginTop: 2, resizeMode: "contain" }}
                    />
                <PickupHeading> Pickup Location</PickupHeading>
                </Button>
                </Pickup >
            < ButtonWrapper disabled={true} onPress={() => { this. toggleModalsecond() } }> 
              <PickupSubHeading>{ order_data.pickup_point }</PickupSubHeading>
            </ButtonWrapper>
            </CourseMetadataWrapper>

            <CourseMetadataWrapper>
            < Pickup>
                <Button iconLeft light transparent >
                <Image
                        source={require('../../images/marketing.png')}
                         style={{ width: 20, height: 20, marginTop: 2, resizeMode: "contain",marginLeft: 40 }}
                    />
                  <StatusHeading> Status </StatusHeading>
                </Button>
                </ Pickup >
            < StatusButtonWrapper style={{backgroundColor: color }} onPress={() => { this.toggleModal() } }> 
              < StatusSubHeading >{ status }</ StatusSubHeading >
            </StatusButtonWrapper>
            </CourseMetadataWrapper>
            </Bottom>
            <Top>
              <CourseMetadataWrapper>
              <SubHeading>Customer Name</SubHeading>
              <Title>{ customer_data.name }</Title>
            </CourseMetadataWrapper>
            
              <CourseMetadataWrapper>
            <SubHeading>Phone</SubHeading>
            <Title>{ customer_data.phone_number }</Title>
              </CourseMetadataWrapper>
              
          </Top>
          <Top>
              <CourseMetadataWrapper>
            <SubHeading style={{marginTop:15}}>Email</SubHeading>
            <Title>{ customer_data.email }</Title>
            </CourseMetadataWrapper>
          </Top>
          <Last>
        </Last>
        <Card>
        <DataTable>
          <DataTable.Header style={{backgroundColor:"#fafafa",borderRadius:8,borderWidth:1,borderColor:'#fafafa',margin:5}}>
            <DataTable.Title style={{ flex:1 }}>SL</DataTable.Title>
            <DataTable.Title style={{ flex:2 }}>Item</DataTable.Title>
            <DataTable.Title style={{ flex:0.5 }}>Qty</DataTable.Title>
            <DataTable.Title style={{ flex:0.5 }}>Price</DataTable.Title>
          </DataTable.Header>

          <FlatList
              data={order_items}
              keyExtractor={item => order_items.indexOf(item).toString()}
              renderItem={({ item }) => (
                this.renderOrderDetails(item, order_items.indexOf(item).toString())
              )}
              showsVerticalScrollIndicator={false}
            />
        </DataTable>
        </Card>
        </Right>

        {!loading &&  typeof pickup_points != 'undefined' && pickup_points.length > 0 && (
        <UpdatePlModal orderId={ order_data.id } items={ pickup_points }
          encOrderId={ this.state.encOrderId }
          show={this.state.isModalshow} hide={(updated) => {
            this.setState({ isModalshow: false });
            if( updated ) {
              this.loadDetails();
            }
          }} />
        )}

        {!loading && typeof pickup_locations != 'undefined' && pickup_locations.length > 0 && (
        <UpdateStatusModal orderId={ order_data.id } items={ pickup_locations }
          encOrderId={ this.state.encOrderId }
          show={this.state.isModalVisible} hide={(updated) => {
            this.setState({ isModalVisible: false });
            if( updated ) {
              this.setState( { statusUpdated: true });
              setTimeout(() => {
                this.setState( { statusUpdated: false });
              }, 2000);
              this.loadDetails();
            }
          }} />
        )}

        {this.state.statusUpdated == true &&
          <StatusUpdateIndicator text="Order status updated" />
        }

      </Wrapper>
  );
  }
}

const mapStateToProps = (state) => {
  return {
      userInfo: state.userReducer.userInfo,
      loggedIn: state.authReducer.loggedIn,
  };
};

export default connect(mapStateToProps)(OrderDetials);
