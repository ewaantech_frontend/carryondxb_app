import React from 'react'
import { defaultStackNavigatorHeaderStyle } from '../styles/common'
import CoursesListView from '../components/list-views/CoursesListView'

export default class  UpcomingList extends React.Component {
  static navigationOptions = {
    title: 'My Referrals',
    headerBackTitle: null,
    ...defaultStackNavigatorHeaderStyle
  }

  render() {
    return (
        <CoursesListView
        loading={this.props.loading}
        items={this.props.items}
        onItemPress={number => this.props.onItemPress(number)}
        onRefresh={() => this.props.onRefresh() }
      />
    )
  }
}