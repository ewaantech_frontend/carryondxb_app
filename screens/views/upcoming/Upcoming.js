import React from 'react'
import { Container } from 'native-base'
import { SafeAreaViewWrapper, CustomStatusBar } from '../components/ui'
import { defaultStackNavigatorHeaderStyle } from '../styles/common'
import { BackHandler } from "react-native";
// import { serviceWrapper } from '../services/serviceWrapper';
// import { cancellablePromise } from "../services/cancellable-promise";
// import { connect } from 'react-redux';
import UpcomingList from './UpcomingList'

class Upcoming extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      formLoading: false,
      orders:
        [{
            'id' :1,
            'qty': 5,
            'price': 250
        }]
    }
  }

  static navigationOptions = {
    title: 'My Referals',
    headerBackTitle: null,
    ...defaultStackNavigatorHeaderStyle
  }
 
  componentDidMount() {
    this.willFocus = this.props.navigation.addListener('willFocus', () => {
      this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
        this.goBack();
        return true;
      });
    });
  }

  goBack = (async() => {
    this.props.navigation.navigate('Referral', {current_route: 'referrals_list'});
  });
  render() {
    const { formLoading, orders } = this.state

    return (
      <SafeAreaViewWrapper>
        <Container>
          <CustomStatusBar />
         < UpcomingList 
         loading={formLoading}
         items={orders}
         onItemPress={number => this.dialCall(number)}
         onRefresh={() => {
           null //this.setState({ formLoading: true }); this.loadReferrals();
         }}
         />
        </Container>
      </SafeAreaViewWrapper>
    )
  }
}

const mapStateToProps = (state) => {
  return {
      userInfo: state.userReducer.userInfo,
      loggedIn: state.authReducer.loggedIn,
  };
};

export default Upcoming;