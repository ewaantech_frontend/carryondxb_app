import React, { Component } from 'react'
import { connect } from 'react-redux';
import { serviceWrapper } from '../../services/serviceWrapper';
import { cancellablePromise } from "../../services/cancellable-promise";
import Modal from "react-native-modal";
import { StyleSheet, View, TouchableOpacity, FlatList, Alert } from 'react-native'
import { Button, Text } from 'native-base';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {  CustomActivityIndicator } from '../components/ui'

class UpdatePlModal extends Component {

    constructor(props) {
        super(props);
        this.state = {
            loading: false
        }
    }

    pendingPromises = [];

    appendPendingPromise = promise =>
        this.pendingPromises = [...this.pendingPromises, promise];

    removePendingPromise = promise =>
        this.pendingPromises = this.pendingPromises.filter(p => p !== promise);

    updateStatus = (location) => {
        
        this.setState({ loading: true});
        const obj = new serviceWrapper();
        const data = {
            pickup: location,
            order_id: this.props.orderId,
            key: this.props.userInfo.key
        }
        const wrappedPromise = cancellablePromise(obj.post('change_pl_status', data));
        this.appendPendingPromise(wrappedPromise);
        wrappedPromise.promise
            .then((res) => {
            this.setState({ loading: false});
            if(res !== 'errors') {
                if(res.status === true) {
                    Alert.alert('Updated successfully');
                    this.props.hide(true);
                } else {
                    ToastAndroid.showWithGravityAndOffset(
                        res.message,
                        ToastAndroid.LONG,
                        ToastAndroid.BOTTOM,
                        25,
                        50,
                    );
                }
            }
        })
        .then(() => this.removePendingPromise(wrappedPromise))
        .catch(errorInfo => {
            this.setState({formLoading: false});
            if (!errorInfo.isCanceled) {
            this.removePendingPromise(wrappedPromise);
            }
        });
    }

    renderLocations = (item) => {
        if( item.name == 'DELIVERED') {
            return null;
        }
        return (
            <Button onPress={() => { this.updateStatus(item.name) }} style={styles.buttonpickup}>
                <Text style={styles.buttontext}>
                    { item.name }
                </Text>
            </Button>
        )
    }

    render() {
        return (
            <View>
            <Modal isVisible={this.props.show}
            onSwipeComplete={() => this.props.hide() }
            swipeDirection={["left", "right", "up", "down"]}
            style={styles.modalMainContent}>
                <View style={styles. modalsecond}>
                    <View style={{ flexDirection: "row"}}>
                        <Text style={styles. text}>
                            Update order pickup location
                        </Text>
                        <TouchableOpacity onPress={() => this.props.hide() }>
                            <Icon name="close-circle-outline" style={{
                            marginLeft: "10%", color: 'black',
                            fontSize: 22, fontWeight: 'bold',
                            top: 3
                            }}>
                        </Icon>
                        </TouchableOpacity>
                    </View>
                    <Text style={styles.rowtext}>
                    { this.props.encOrderId }
                    </Text>
                    <View style={styles.row}>
                        <FlatList
                            style={{margin: 5}}
                            columnWrapperStyle={styles.flatrow}
                            numColumns={2} 
                            data={this.props.items}
                            keyExtractor={item => this.props.items.indexOf(item).toString()}
                            renderItem={({ item }) => (
                                this.renderLocations(item)
                            )}
                            showsVerticalScrollIndicator={false}
                        />
                    </View>
                </View>
            </Modal>

            {this.state.loading && <CustomActivityIndicator />}
            </View>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        userInfo: state.userReducer.userInfo,
        loggedIn: state.authReducer.loggedIn,
    };
  };
  
export default connect(mapStateToProps)(UpdatePlModal);


const styles = StyleSheet.create({
    text:{
      fontFamily: 'Montserrat-Medium',
      fontSize: 17,
      color: "#434a5e",
      paddingLeft:5,
    },
    flatrow: {
        flex: 1,
        justifyContent: "space-around"
    },
    row: {
      flexDirection: "row",
      marginTop:20,
    },
    rowtext: {
      fontFamily: 'Montserrat-Medium',
      fontSize: 17,
      color: "#434a5e",
      paddingLeft:5,
      marginTop:14,
    },
    buttontext: {
      fontFamily: 'Montserrat-Medium',
      fontSize: 12,
      color: "#ffffff",
     paddingLeft:5,
     textAlign: "center"
    },
    modalContent: {
      backgroundColor: 'white',
      padding: 15,
      borderRadius: 4,
      borderColor: 'rgba(0, 0, 0, 0.1)'
    },
    modalMainContent: {
      justifyContent: 'flex-end',
      margin: 0,
    },
    buttonrow: {
      backgroundColor: '#f25f1b',
      padding: 12,
    //   marginLeft:5,
      borderRadius: 2,
      width:105,
      marginBottom:2,
    },
    buttonpickup: {
      backgroundColor: '#f25f1b',
      padding: 12,
      borderRadius: 2,
      width:"48%",
      marginTop:10,
    },
    button: {
      backgroundColor: '#f25f1b',
      padding: 12,
      marginLeft:5,
      borderRadius: 2,
      width:170,
      marginTop:20,
    },
    bar: {
       padding: 0,
       backgroundColor: 'white',
       borderRadius: 4,
       borderColor: 'rgba(0, 0, 0, 0.1)'
    },
    modalsecond: {
      backgroundColor: 'white',
       padding: 22,
      borderRadius: 4,
      borderColor: 'rgba(0, 0, 0, 0.1)'
    },
  
  })