import React, { Component } from 'react';
import { Card } from 'native-base'
import styled from 'styled-components/native'
import { Content, Button,  Text } from 'native-base';
 import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
 import {  StyleSheet, View ,Image} from 'react-native';
import {  FlatList } from 'react-native';
import { ListLoadingComponent, ListEmptyComponent, StatusUpdateIndicator } from '../components/ui'
import { connect } from 'react-redux';
import { serviceWrapper } from '../../services/serviceWrapper';
import { cancellablePromise } from "../../services/cancellable-promise";
import moment from 'moment';
import UpdatePlModal from '../modals/UpdatePlModal'
import UpdateStatusModal from '../modals/UpdateStatusModal'
 
const Wrapper = styled(Card)`
  flex-direction: row;
  height:270;
  padding: 16px;
  margin-top: 16;
  border-top-color: #f4f6fa;
  width: 98%;
`

export const Right = styled.View`
  flex: 1;
  margin-left: 1%;
`

export const Top = styled.View`
  flex-direction: row;
`

export const DetailsWrapper = styled.TouchableOpacity`
  flex: 1;
`

export const Title = styled.Text.attrs({
  numberOfLines: 1
})`
  font-family: 'Montserrat-Medium';
  font-size: 13;
  margin-right: 8;
  color:#434a5e ;
`

export const SubHeading = styled.Text`
  font-family: 'Montserrat-Regular';
  font-size: 12;
  color:#a7adbf  ;
  margin-top: 4;
`
export const Pickup = styled.View`

`
export const PickupHeading = styled.Text`
  font-family: 'Montserrat-Regular';
  font-size: 12;
  margin-top: 4;
  color:#a7adbf;
  margin-Right: 40;
  `
export const StatusHeading = styled.Text`
  font-family: 'Montserrat-Regular';
  font-size: 12;
  margin-Right: 50;
  margin-top: 4;
  color:#a7adbf;
  `

export const Statustext = styled.Text`
  font-family: 'SFProTextRegular';
  font-size: 15;
  margin-left: 60;
  margin-top: 2;
  color:#a7adbf;
`
export const PickupSubHeading = styled.Text`
font-family: 'Montserrat-Medium';
  font-size: 15;
  color: #ffffff;
  margin-top: 7;
  margin-left: 15;
`
export const StatusSubHeading = styled.Text`
font-family: 'Montserrat-Medium';
  font-size: 15;
  color: #ffffff;
  margin-top: 7;
  text-align: center;
`
export const Buttontext = styled.Text`
font-family: 'Montserrat-Medium';
  font-size: 15;
  color: #1972b5;
`
export const Printtext = styled.Text`
font-family: 'Montserrat-Medium';
  font-size: 15;
  color: #434a5e;
`
export const Bottom = styled.View`
  flex-direction: row;
  margin-top: 16;
`
const MetaWrapper = styled.View`
  margin-top: 2;
  border-color: transparent;
  border-top-color: #dee3ea;
  border-width: 1;
`

const CourseMetadataWrapper = styled.View`
  flex: 1;
`
export const ButtonWrapper = styled.TouchableOpacity`
background: #0059b3;
border-radius:2;
margin-top: 8;
height:30%;
color:#1972b5;
`
export const StatusButtonWrapper = styled.TouchableOpacity`
border-radius:2;
margin-top: 8;
margin-left:12;
height:30%;
width:95%
color:#61b4ff;
`
export const PrintDetailsWrapper = styled.View`
`

export const ViewDetailsTextWrapper = styled.View`
margin-left: 70;
`

export const Last = styled.View`
  flex-direction: row;
  margin-top: 10;
`
export const ListWrapper = styled.View`
  flex: 1;
`
class PickupListScreen extends Component{

  constructor(props) {
    super(props);
  }
  state = {
    isModalVisible: false,
    isModalshow: false,
    refreshing: false,
    items: [],
    loading: true,
    statusChangeOrderId: '',
    encOrderId: '',
    pickup_points: [],
    pickup_locations: [],
    statusUpdated: false
  };

  toggleModal = (orderId, encOrderId) => {
    this.setState({ isModalVisible: !this.state.isModalVisible, statusChangeOrderId: orderId, encOrderId: encOrderId });
  };
  toggleModalsecond = (orderId, encOrderId) => {
    this.setState({ isModalshow: !this.state.isModalshow, statusChangeOrderId: orderId, encOrderId: encOrderId });
  };

  componentDidMount() {
    this.willFocus = this.props.navigation.addListener('willFocus', () => {
      this.list_oders();
    });
    this.list_oders();

    const that = this;
    const { navigation } = this.props;
    let updateApiInterval = null;
    this.focusListener = navigation.addListener("didFocus", () => {
      updateApiInterval = setInterval(function() {
        that.list_oders(true);
      }, 30000);
    });
    // Clear interval
    this.focusListener = navigation.addListener("didBlur", () => {
      console.debug('Stop');
      clearInterval(updateApiInterval);
    });
  }

  componentDidUpdate(nextProps) {
    const { searchTerm, refreshingCtrl } = this.props
    if (nextProps.searchTerm !== searchTerm || nextProps.refreshingCtrl !== refreshingCtrl) {
      this.list_oders(true);
    }
   }

  pendingPromises = [];

  appendPendingPromise = promise =>
    this.pendingPromises = [...this.pendingPromises, promise];

  removePendingPromise = promise =>
    this.pendingPromises = this.pendingPromises.filter(p => p !== promise);

  renderOrders = (item, navigation) => {
    let date = '';
    if(item[2] != '' && item[2] != null) {
      date = moment(item[2])
                .utcOffset('+04:00')
                .format('MMM DD HH.mm A.');
    } else {
      date = 'Jan 19';
    }
    const is_enabled = this.props.userInfo.is_defalut_user != 'YES';
    let color = "#80bfff";
    if( item[4] == 'In Kitchen' ) {
      color = "orange";
    }
    return (
      <Wrapper onPress={() => onPress()}>
      <Right>
        <Top>
            <CourseMetadataWrapper>
            <SubHeading>Order ID</SubHeading>
            <Title>{item[0]}</Title>
            </CourseMetadataWrapper>
            <CourseMetadataWrapper>
            <SubHeading>Flight</SubHeading>
            <Title>{item[1]}</Title>
            </CourseMetadataWrapper>
            <CourseMetadataWrapper>
            <SubHeading>Pickup Time</SubHeading>
            <Title>{date}</Title>
            </CourseMetadataWrapper>
        </Top>
          <Bottom>
                <CourseMetadataWrapper>
                <Pickup >
                <Button iconLeft light transparent >
                <Image
                        source={require('../../images/pin.png')}
                         style={{ width: 20, height: 20, marginTop: 2, resizeMode: "contain" }}
                    />
                <PickupHeading> Pickup Location</PickupHeading>
                </Button>
                </Pickup >
              < ButtonWrapper disabled={is_enabled} onPress={() => { this. toggleModalsecond(item[5], item[0]) } }> 
              <PickupSubHeading>{item[3]}</PickupSubHeading>
              </ButtonWrapper>
                </CourseMetadataWrapper>

                <CourseMetadataWrapper>
                < Pickup>
                
                <Button iconLeft light transparent >
                <Image
                        source={require('../../images/marketing.png')}
                         style={{ width: 20, height: 20, marginTop: 2, resizeMode: "contain",marginLeft: 40 }}
                    />
                  <StatusHeading> Status </StatusHeading>
                </Button>
                </ Pickup >
            
              < StatusButtonWrapper style={{ backgroundColor: color }} onPress={() => { this.toggleModal(item[5], item[0]) } }> 
              < StatusSubHeading >{item[4]}</ StatusSubHeading >
              </StatusButtonWrapper>
              </CourseMetadataWrapper>
          </Bottom>
          <MetaWrapper/>
         < Last>
         <PrintDetailsWrapper >
          <Button iconLeft light transparent style={{ backgroundColor:"#fafafa", }}
          onPress={() => alert('Login with Facebook')} >
             <Image
                        source={require('../../images/print.png')}
                        style={{ width: 20, height: 20, marginTop: 2, resizeMode: "contain" }}
                    />
            <Printtext> Print Invoice</Printtext>
          </Button>
          </PrintDetailsWrapper > 

          <ViewDetailsTextWrapper>
            <Content>
              <Button iconRight transparent Primary
                onPress={() => this.viewDetails(navigation, item[5], item[0]) } >
                <Buttontext >View details</Buttontext>
                <Icon name='menu-right'  size={30} color="#1972b5"/>
              </Button>
            </Content>
          </ViewDetailsTextWrapper> 
            </Last>
      </Right>
</Wrapper>
 );
}

viewDetails = (navigation, OrderId, encOrderId) => {
  navigation.navigate('order_details', {
    orderId: OrderId, 
    encOrderId: encOrderId,
    pickup_points: this.state.pickup_points, 
    pickup_locations: this.state.pickup_points
  });
}

  onRefresh = () => {
    this.list_oders();
  }

  list_oders = () => {
    console.debug('here');
    this.setState({ loading: true});
    const obj = new serviceWrapper();
    const data = {
      status_type: 'READY_FOR_PICKUP',
      key: this.props.userInfo.key,
      searchTerm: this.props.searchTerm
    }
    const wrappedPromise = cancellablePromise(obj.post('orders', data));
    this.appendPendingPromise(wrappedPromise);
    wrappedPromise.promise
      .then((res) => {
        this.setState({ loading: false});
        if(res !== 'errors') {
          if(res.status === true) {
            res.pickup_points.push({name: 'DELIVERED'})
            this.setState({ items: res.aaData, pickup_points: res.pickup_points, pickup_locations: res.pickup_points });
          } else {
            ToastAndroid.showWithGravityAndOffset(
              res.message,
              ToastAndroid.LONG,
              ToastAndroid.BOTTOM,
              25,
              50,
            );
          }
        }
      })
      .then(() => this.removePendingPromise(wrappedPromise))
    .catch(errorInfo => {
      this.setState({formLoading: false});
      if (!errorInfo.isCanceled) {
        this.removePendingPromise(wrappedPromise);
      }
    });
  }

  render() {
    const { refreshing, items, loading, pickup_points, pickup_locations } = this.state
    return (
      <View>
      <ListWrapper>
          {loading && <ListLoadingComponent text="Loading Orders" />}

          {!loading && items.length === 0 && (
            <ListEmptyComponent
              showRefreshButton
              text="No orders to display."
              onRefreshButtonPress={() => this.onRefresh()}
            />
          )}

          {!loading && items.length > 0 && (
            <FlatList
              style={{ flex: 1 }}
              data={items}
              keyExtractor={item => items.indexOf(item).toString()}
              renderItem={({ item }) => (
                this.renderOrders(item, this.props.navigation)
              )}
              
              showsVerticalScrollIndicator={false}
            />
          )}
        </ListWrapper>

        {!loading && pickup_points.length > 0 && (
        <UpdatePlModal orderId={this.state.statusChangeOrderId} items={ pickup_points }
          encOrderId={ this.state.encOrderId }
          show={this.state.isModalshow} hide={(updated) => {
            this.setState({ isModalshow: false });
            if( updated ) {
              this.setState( { statusUpdated: true });
              setTimeout(() => {
                this.setState( { statusUpdated: false });
              }, 1000);
              this.list_oders();
            }
          }} />
        )}

        {!loading && pickup_locations.length > 0 && (
        <UpdateStatusModal orderId={this.state.statusChangeOrderId} items={ pickup_locations }
          encOrderId={ this.state.encOrderId }
          show={this.state.isModalVisible} hide={(updated) => {
            this.setState({ isModalVisible: false });
            if( updated ) {
              this.setState( { statusUpdated: true });
              setTimeout(() => {
                this.setState( { statusUpdated: false });
              }, 2000);
              this.list_oders();
            }
          }} />
        )}

          {this.state.statusUpdated == true &&
            <StatusUpdateIndicator text="Order status updated" />
          }

        </View>
    );
  }
}

const mapStateToProps = (state) => {
  // Redux Store --> Component
  return {
      userInfo: state.userReducer.userInfo,
      loggedIn: state.authReducer.loggedIn,
  };
};

export default connect(mapStateToProps)(PickupListScreen);


const styles = StyleSheet.create({

  text:{
    fontFamily: 'Montserrat-Medium',
    fontSize: 17,
    color: "#434a5e",
    paddingLeft:5,
   // paddingRight:15,
    // lineHeight: 15,
    // paddingBottom:20,
    
  },

  row: {
    flexDirection: "row",
    marginTop:20,
    //justifyContent:"space-between"
  },
  rowtext: {
    fontFamily: 'Montserrat-Medium',
    fontSize: 17,
    color: "#434a5e",
    paddingLeft:5,
    marginTop:14,
  },
  buttontext: {
    fontFamily: 'Montserrat-Medium',
    fontSize: 12,
    color: "#ffffff",
   paddingLeft:5,
   textAlign: "center"
  },
  modalContent: {
    backgroundColor: 'white',
     padding: 15,
    borderRadius: 4,
    borderColor: 'rgba(0, 0, 0, 0.1)'
  },
  modalMainContent: {
    justifyContent: 'flex-end',
    margin: 0,
  },
  buttonrow: {
    backgroundColor: '#f25f1b',
    padding: 12,
    marginLeft:5,
    borderRadius: 2,
    width:105,
    marginBottom:2,
    
  },
  
  buttonpickup: {
    backgroundColor: '#61b4ff',
    padding: 12,
    marginLeft:30,
    borderRadius: 2,
    width:170,
    marginTop:10,
    
  },
  button: {
    backgroundColor: '#f25f1b',
    padding: 12,
    marginLeft:5,
    borderRadius: 2,
    width:170,
    marginTop:20,
    
  },
  bar: {
   
     padding: 0,
     backgroundColor: 'white',
     borderRadius: 4,
     borderColor: 'rgba(0, 0, 0, 0.1)'
  },
  modalsecond: {
    backgroundColor: 'white',
     padding: 22,
     //marginLeft:5,
    borderRadius: 4,
    borderColor: 'rgba(0, 0, 0, 0.1)'
  },

})