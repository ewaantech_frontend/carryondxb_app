import React from 'react'
import { Platform } from 'react-native'
import styled from 'styled-components'
// import { Ionicons } from '@expo/vector-icons'
import Icon from "react-native-vector-icons/Ionicons";
import { StyleSheet, View } from "react-native";


const HeaderBackImageWrapper = styled.View`
  width: 35;
  justify-content: center;
  align-items: center;
  margin-left: ${Platform.OS === 'ios' ? 8 : 0};
`

export const safeAreaViewStyle = {
  flex: 1
}

export const onboardingHeaderStyle = {
  backgroundColor: '#ffff',
  borderBottomColor: 'transparent'
}

const headerTitleStyle = {
  fontFamily: 'SFProDisplayBold',
  fontSize: 20,
  color: '#000'
}
export const defaultStackNavigatorHeaderStyle = {
  headerStyle: {
    backgroundColor: '#fff',
    borderBottomColor: 'transparent'
  },
  headerTitleStyle,
  headerTintColor: '#32ce89',
  headerBackTitleStyle: headerTitleStyle,
  headerBackImage: ({ tintColor }) => (
    <HeaderBackImageWrapper>
      <Icon name="md-arrow-round-back" color={tintColor} size={30} />
    </HeaderBackImageWrapper>
  )
}

export const tabHeaderStyle = {
  backgroundColor: '#fff'
}

export const tabBarOptions = {
    activeTintColor: 'darkgreen',
    inactiveTintColor: 'grey',
    showLabel: true,
    style: {
      backgroundColor: '#fff',
      borderTopColor: '#dee3ea'
    }
}

export const Barstyle = ({ barStyle }) => {
  return (
    <View style={BarStyles.barContainer}>
      <View style={[BarStyles.bar, barStyle]} />
    </View>
  );
};

const BarStyles = StyleSheet.create({
  barContainer: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center"
  },
  bar: {
    width: "30%",
    height: 4,
    borderRadius: 5,
    marginTop: 5,
    marginBottom: 10,
    backgroundColor: "#e2e2e2"
  }
});
