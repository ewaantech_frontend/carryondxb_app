import React from 'react';
import { View, Text, ImageBackground, StyleSheet, Image, KeyboardAvoidingView,
        TouchableOpacity } from 'react-native';
import { heightPercentageToDP as hp, widthPercentageToDP as wp, } from 'react-native-responsive-screen';
import { Item, Input, Icon, Label, Content, Title, Body, Button } from 'native-base';

class Passwordresetmessage extends React.Component {

    static navigationOptions = {
        headerShown: false
      }

    state = {
        icon: "eye-off",
        password: true
    };

    _changeIcon() {
        this.setState(prevState => ({
            icon: prevState.icon === 'eye' ? 'eye-off' : 'eye',
            password: !prevState.password
        }));
    }

    login = () => {
        console.debug('login')
        this.props.navigation.navigate('Upcoming');
    }

    render() {
        const { label, icon, onChange } = this.props;
        return (
            <KeyboardAvoidingView style={{ flex: 1 }} behavior="padding">
                    <View style={styles.MainContainer}>
                  
                    <Image
                        source={require('../..//images/logo.png')}
                        style={{ width: 200, height: 60, marginTop: 90, resizeMode: "contain" }}
                    />
                    </View>

                    <View style={{flex:1}}>
                        <View style={styles.bottomView}>
                        <Text style={{fontWeight: "bold"}}> Thank you !</Text>

                        <Text style={{ marginTop: "8%"}}> Your password reset has benn done.</Text>
                        <Text style={{ marginTop: "2%"}}> Try login with email and new password. </Text>
                            
                        </View>
                        <View style={styles.bottombutton}>
                        <TouchableOpacity onPress={() => {
                                this.login()
                            }} style={{
                                backgroundColor: "#D35211",
                                width: "80%",
                                height: 50,
                            }}>
                                <Text style={{
                                    color: "#fff", alignSelf: "center",
                                    fontSize: 15,
                                    marginTop: 10
                                    }}>GO TO LOCATION </Text>
                            </TouchableOpacity>
                            </View>
                    </View>
            </KeyboardAvoidingView>
        );
    }
}

export default Passwordresetmessage;

const styles = StyleSheet.create({
    MainContainer: {
        flex: 1,
        alignItems: 'center',
        height: "50%",
       
    },
    TextStyle: {
        color: '#0250a3',
        textAlign: 'center',
        fontSize: hp('4.2%'),
        marginTop: 10,
    },
    bottomView: {
       
        width: wp('95%'),
        height: hp('40%'),
        //backgroundColor: '#FFFFFF',
        justifyContent: 'center',
        alignItems: 'center',
        bottom: 0,
        shadowColor: "red",
        shadowOpacity: 0.8,
        shadowRadius: 2,
        shadowOffset: {
            height: 1,
            width: 0
        },
        // borderWidth:1,
        borderColor: "grey",
        marginLeft:"2.5%",
        marginRight:"2.5%",
        marginTop:-45,
    },
    bottombutton:{
        width: wp('95%'),
        height: hp('40%'),
        //backgroundColor: '#FFFFFF',
        justifyContent: 'center',
        alignItems: 'center',
        bottom: 0,
        shadowColor: "red",
        shadowOpacity: 0.8,
        shadowRadius: 2,
        shadowOffset: {
            height: 1,
            width: 0
        },
        // borderWidth:1,
        borderColor: "grey",
        marginLeft:"2.5%",
        marginRight:"2.5%",
        marginTop:-80,
    },
    textStyle: {
        color: '#fff',
        fontSize: 22
    }
});