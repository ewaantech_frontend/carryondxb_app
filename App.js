import 'react-native-gesture-handler';
import React, { Component } from 'react'
import AppContainer from './screens/routes'
import { Text } from 'react-native'

export default class App extends Component {

  constructor() {
    super()
    if (Text.defaultProps == null) {
      Text.defaultProps = {};
      Text.defaultProps.allowFontScaling = false;
    }
  }

  render() {
    return <AppContainer />
  }
}