import React, { Component } from 'react'
import { AppLoading, registerRootComponent } from 'expo'
import * as Font from 'expo-font'
import AppContainer from 'routes'
import StorybookUI from '../storybook'

class App extends Component {
  state = {
    fontLoaded: false,
    storybook: false
  }

  async componentWillMount() {
    await Font.loadAsync({
      Customfont: require('assets/fonts/COMIC.TTF'),
      MontRegular: require('assets/fonts/Montserrat-Regular.ttf'),
      MontMedium: require('assets/fonts/Montserrat-Medium.ttf'),

      })

    this.setState({
      fontLoaded: true
    })
  }

  render() {
    const { fontLoaded } = this.state

   
  }
}

registerRootComponent(App)
