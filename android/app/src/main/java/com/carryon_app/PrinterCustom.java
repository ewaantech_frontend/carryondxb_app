package com.carryon_app;

import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.uimanager.IllegalViewOperationException;
import com.facebook.react.bridge.LifecycleEventListener;
import com.facebook.react.modules.core.DeviceEventManagerModule;

import com.facebook.react.modules.core.DeviceEventManagerModule.RCTDeviceEventEmitter;

public class PrinterCustom extends ReactContextBaseJavaModule implements LifecycleEventListener {
    private static ReactApplicationContext reactContexts;
    // private PrintAction mPrint = null;
    private Scanner mScan = null;

    public PrinterCustom(ReactApplicationContext reactContext) {
        super(reactContext);
        this.reactContexts = reactContext;
        reactContext.addLifecycleEventListener(this);
    }

    @Override
    public String getName() {
        return "PrinterCustom";
    }

    @Override
    public void onHostResume() {

    }

    @Override
    public void onHostPause() {

    }

    @Override
    public void onHostDestroy() {
        // mPrint.destroy();
        
    }

    // @ReactMethod
    // public void printItem(Callback errorCallback, Callback successCallback) {
    //     try {
    //         mPrint = new PrintAction(reactContexts);
    //         mPrint.printMyItem();
    //         successCallback.invoke("Callback : Print job completed");
    //     } catch (IllegalViewOperationException e) {
    //         errorCallback.invoke(e.getMessage());
    //     }
    // }

    // @ReactMethod
    // public void printBarcode(Callback errorCallback, Callback successCallback) {
    //     try {
    //         mPrint = new PrintAction(reactContexts);
    //         mPrint.printBarcode();
    //         successCallback.invoke("Callback : Print barcode completed");
    //     } catch (IllegalViewOperationException e) {
    //         errorCallback.invoke(e.getMessage());
    //     }
    // }

    @ReactMethod
    public void scanCode(Callback errorCallback, Callback successCallback) {
        try {
            mScan = new Scanner(reactContexts);
            mScan.scanContentSet();
            successCallback.invoke("Callback : Scan barcode completed");
        } catch (IllegalViewOperationException e) {
            errorCallback.invoke(e.getMessage());
        }
    }

    @ReactMethod
    public void removeScanCode(Callback errorCallback, Callback successCallback) {
        try {
            mScan = new Scanner(reactContexts);
            reactContexts.getJSModule(RCTDeviceEventEmitter.class).emit("barcode", "destroy");
            mScan.unRegister();
            successCallback.invoke("Callback : Scan barcode removed");
        } catch (IllegalViewOperationException e) {
            errorCallback.invoke(e.getMessage());
        }
    }

}