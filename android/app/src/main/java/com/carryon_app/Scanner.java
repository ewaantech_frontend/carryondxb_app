package com.carryon_app;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.device.ScanDevice;

import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.modules.core.DeviceEventManagerModule.RCTDeviceEventEmitter;

public class Scanner {
	ScanDevice sm;
	private final static String SCAN_ACTION = "scan.rcv.message";
	private String barcodeStr;
    private ReactApplicationContext rContext = null;

    Scanner(ReactApplicationContext ctxt) {
    	if( this.rContext != null ) {
    		return;
		}
        this.rContext = ctxt;
    }

    public void scanContentSet() {
		sm = new ScanDevice();
		sm.setOutScanMode(0);
		IntentFilter filter = new IntentFilter();
		filter.addAction(SCAN_ACTION);
		rContext.registerReceiver(mScanReceiver, filter);

		sm.openScan();
		//sm.startScan();
	}

	private BroadcastReceiver mScanReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub

			byte[] barocode = intent.getByteArrayExtra("barocode");
			int barocodelen = intent.getIntExtra("length", 0);
			byte temp = intent.getByteExtra("barcodeType", (byte) 0);
			byte[] aimid = intent.getByteArrayExtra("aimid");
			barcodeStr = new String(barocode, 0, barocodelen);
			unRegister();
			scanContentSet();
			rContext.getJSModule(RCTDeviceEventEmitter.class).emit("barcode", barcodeStr);
		}

	};


	public void unRegister() {
		rContext.getJSModule(RCTDeviceEventEmitter.class).emit("barcode", "1");
		if (sm != null) {
			sm.stopScan();
		}
		rContext.getJSModule(RCTDeviceEventEmitter.class).emit("barcode", "2");
		rContext.unregisterReceiver(mScanReceiver);
	}

}